
# GIT Sites
1. [Overleaf step-by-step manual on GIT and Overleaf](https://www.overleaf.com/learn/how-to/How_do_I_push_a_new_project_to_Overleaf_via_git%3F)
1. [Overleaf blog on SSO accounts and GIT and Overleaf](https://www.overleaf.com/blog/195-new-collaborate-online-and-offline-with-overleaf-and-git-beta)
1. [GitHub on credential helper](https://help.github.com/en/articles/caching-your-github-password-in-git)
1. [Some handy GiT examples by Marohn](http://marohn-public.site44.com/Marohn-20180320-170601--Overleaf-and-ShareLaTeX-and-Github.html)

```git config --global credential.helper 'cache --timeout=3600'```

