%!TEX root =  ../main.tex

\newpage
\section{Organisation Design}
\label{sec:organisation-design}

When an organisation is purposeful,
and it is intentionally designed 
then the organisation design plays
an important role
\citetext{\citealp[p. 8]{book-hoogervorst-2017};
\citealp[p. 94]{article-dietz-2013};
\citealp{book-daft-2010}}. 
A valuable extension 
to the description of an 
\nameref{def:organisation} 
(\ref{def:organisation}) 
is therefore a description of the main functions 
within an organisation.
An organisation consists of three main functions 
that should be addressed in the design of the organisation  
\citep[p. 48]{book-janssen-2015}.
\begin{enumerate}
    \item Governance
    \item Run (Execution) 
    \item Change (Implementation \& Improvement)
\end{enumerate}

\subsection{Organisations are in non-stop change from the outside}
\label{sec:non-stop-change}

It can be argued that the run and change are one and the same.
Since organisations are operating in a context that is 
changing highly frequent 
\citep{book-hutchins-2018}. 
Making sense of the high frequent
changing context is very difficult, 
resulting in 
changed interpretations, 
which result in an even higher change frequency 
for the organisation. 

In section \ref{def:vuca} 
(\nameref{def:vuca})
I address the 
volatility, uncertainty, complexity and ambiguity (VUCA) 
of the current world.
In section \ref{def:cynefin} 
(\nameref{def:cynefin})
I will introduce the Cynefin framework 
by Dave Snowden.
The Cynefin framework can be used to make sense 
of the context per sub-system of the organisation.

\subsection{Organisations are in non-stop change from within}

Change in the organisational context
is from the inside also non-stop, 
since people within the organisation change non-stop
\citep{book-hoogervorst-2017, book-hutchins-2018}. 


In section \ref{intro:where} 
(\nameref{intro:where})
I addressed 
the challenges for organisation design in the domain of Risk Management.


\subsection{Reductionistic approach versus a holistic approach}
There are various ways to look at the world. 
A classical dissection of ways to look 
at the world is one of reductionistic versus holistic.

Reductionism
is 
`any of several related philosophical ideas 
regarding the associations between phenomena 
which can be described in terms of other 
simpler or more fundamental phenomena` 
\citep{wiki:reductionism}.

The holistic
view is 
`the idea that various systems \ldots 
should be viewed as wholes, not merely as a collection of parts`
\citep{wiki:holism}.

In business management the approach by Taylor (Scientific 
Management\footnote{\url{https://en.wikipedia.org/wiki/Scientific_management}
\citep{wiki:taylorism}})
is an example of a reductionistic approach 
to look at the management (and design) of an organisation.
See figure \ref{fig:reductionism} for a 
visualisation of the two approaches.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.7\linewidth]{reductionism-holism}
    \caption{Reductionistic and Holistic approach.}
    \label{fig:reductionism}
\end{figure}


\subsection{Organisation design should not be done in a reductionist approach}

It can be questioned if the holistic approach
or the reductionistic approach is best 
in the effort of designing an organisation.
There is a large set of literature 
that argues that the reductionistic approach is not
the best approach for designing an organisation
\citep{article-makridakis-2009-forecasting, book-hoogervorst-2017, article-dietz-2013, book-janssen-2015, article-dahlberg-2015, book-taleb-2012}.

\subsection{Change hurts the reductionist designed system}

Systems have a tendency to be fragile 
\citep[p. 162]{article-johnson-2013-cas}. 
IT systems are fragile 
\citetext{\citealp{article-lehman-1980};
\citealp[p. 55]{book-taleb-2012};
\citealp[p. 17]{article-dahlberg-2015}
}. 
Organisms are antifragile 
\citep[p. 55]{book-taleb-2012}.
This leads to the conclusion that using 
scientific management (reductionism) 
to design organisation leads to fragile organisations 
\citep[p. 11]{article-dahlberg-2015}.

That non-stop change leads to fragility is also something 
\cite{article-lehman-1980} stated, in the context of software,
long before the concept of antifragility.
\begin{quote} ``As an evolving program 
	is continually changed, 
    its complexity, reflecting deteriorating structure, 
    increases unless work is done to maintain or reduce it.`` 
    \\ - \citep{article-lehman-1996, article-lehman-1980}
\end{quote}

\cite{book-taleb-2012} states also what Lehman stated and 
put this in context with how nature is reacting on change.

\begin{quote} ``\ldots machines are harmed by low-level stressors 
    (material fatigue), 
    organisms are harmed by the absence of low-level stressors 
    (hormesis) \ldots`` 
    \\ - \citep[p. 55]{book-taleb-2012}
\end{quote}

\cite{article-dahlberg-2015} rephrases the words of \cite{book-taleb-2012}
in such a way the
application is clearer to us, or at least to me.
\begin{quote} ``While the mechanical needs 
	continuous repair and maintenance,
     dislikes randomness, and ages with use, 
     the organic is self-healing, loves randomness 
     (in the form of small variations), and ages with disuse.`` 
    \\ - \citetext{\citealp[p. 17]{article-dahlberg-2015}; \citealp[p.59]{book-taleb-2012}}
\end{quote}


\subsection{The reductionist approach to a complex system}

An organisation is a specific type of a Complex Adaptive System 
\citep[p. 11, 93-94]{book-dietz-2012}.
\cite{article-dahlberg-2015} 
conclusion is  
that a reductionistic approach 
to a complex system will be without fruit.

\begin{quote} ``The reductionist approach to a complex system 
    will never bear fruit. 
    Likewise, traditional command and control-style management 
    approaches are impossible to implement in the complex domain.`` 
    \\- \citep[p. 11]{article-dahlberg-2015}
\end{quote}

Therefore, reductionistic approach 
to an organisation will be without fruit.

\begin{quote} ``Enterprises are 
	complex adaptive systems 
	whereby it is impossible to determine 
	the ultimate (operational) reality 
	of the enterprise down to the minute details.`` 
	 \\ - \citep[p. 93]{article-dietz-2013} 
\end{quote}

The field of Enterprise Engineering, 
underwrites the relevance of finding a way to deal with
chaos, complexity and non-stop change. This for me is an 
extra reason that this research is relevant for the field of 
Enterprise Engineering.

\subsection{Enterprise Engineering, antifragility and the black swan}
\label{sec:ee-af-blackswan}

The relevance of the research on antifragility goes beyond
the theme of non-stop change and unpredictability. 
The field of Enterprise Engineering also defines 
the importance of dealing with black swans (X-events).

\begin{quote} ``This type of learning 
	acknowledges the non-planned, emerging character 
	of many enterprise developments 
    \citep{book-hoogervorst-2009}. 
	Hence, 
	employee involvement and participation are essential 
	for addressing 
	enterprise dynamics, complexity, and uncertainty. 
	Enterprise change, hence redesign, 
	is thus fuelled by enterprise learning. 
	As \cite{book-weick-2001} observes, 
	redesign is a continuous activity 
	whereby 
	\textbf{the responsibility for (re)design 
	is dispersed and rests with 
	enterprise members 
	who are coping with the ‘unexpected’}.'' 
	\\ - \citep[p. 91]{article-dietz-2013}
\end{quote}

Black swans are the reason \cite{book-taleb-2012} 
introduced antifragility. 
Both Enterprise Engineering and antifragility recognise
black swans as something that is `unexpected`.

Antifragile states that change by a black swan (X-event) 
are unpredictable. 
Run and change should be designed in such a way 
that the unpredictable change is supported.
This needs a (new and) fitting governance paradigm.
The field of antifragility is a great addition 
to the field of Enterprise Engineering 
on how to design a system that can survive black swan events.

\newpage
\subsection{Reductionistic design is being replaced by holistic design}

The same reasoning that reductionistic design of organisations 
is not the way to go, is also found in a recent report of McKinsey \& Company.
This report is based on a survey and expert input.

In this report the agile organisation 
is re-framed as an antifragile organisation
\citep{artile-aghina-2017}. 
See figure \ref{fig:mckinsey} for a representation 
that should address issues business leaders face 
and are relevant in the
decision on how to design the organisation.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.8\linewidth]{mckinsey}
    \caption{The agile organisation is dawning 
    as the new dominant organisational paradigm 
    by
    \cite{artile-aghina-2017}.}
    \label{fig:mckinsey}
\end{figure}

McKinsey identified, based on a survey and expert input, 
five trademarks of agile (antifragile) organisations. 
See figure \ref{fig:trademarks} for these five trademarks.

I find it interesting that they added technology as trademark
to the list. 
I placed the analysis of this report by McKinsey 
out of scope of this thesis because of timing constraints. 
I do like to include it briefly here because
it shows that the topic of my thesis and the outcome of it 
is relevant to the current business leaders.

\newpage
\begin{figure}[ht]
    \centering
    \includegraphics[width=1\linewidth]{trademarks}
    \caption{There are five trademarks of agile organisations 
    by
    \cite{artile-aghina-2017}.}
    \label{fig:trademarks}
\end{figure}


