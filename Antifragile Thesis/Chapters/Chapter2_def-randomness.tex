%!TEX root =  ../main.tex

\newpage
\section{Randomness}
\label{def:randomness}
\label{intro:chaos}

\subsection{Two types of randomness}

\cite{article-risk-Derbyshire-2014} 
states that there are two types of randomness:

\begin{enumerate}
    \item `epistemological randomness` or `deterministic chaos` 
    \\ It is impossible to know everything and therefore 
    it is impossible to predict the future.
    \\
    \\``Uncertainty results from the inadequacy of the procedures 
    we use to uncover cause, which can have limited efficacy, 
    even ex-post.`` 
    \\ - \citep[p. 216]{article-risk-Derbyshire-2014} 
     
    \item ‘ontological randomness’ or `true randomness`
    \\The change in the now is no guarantee on a certain outcome.
    \\
    \\``A particular combination of factors may result 
    in one outcome on one occasion 
    and a completely different outcome on another``  
    \\ - \citep[p. 216]{article-risk-Derbyshire-2014} 
\end{enumerate}

Both type of randomness can be called chaos or chaotic.

\subsection{What is chaos?}
\label{def:chaos}

Chaos is a word often used when things get complicated.
Edward 
Lorenz\footnote{\url{https://en.wikipedia.org/wiki/Edward_Norton_Lorenz}
\citep{wiki:Edward_Norton_Lorenz}
},
recognised as one of the 
founders and pioneers of the 
chaos 
theory\footnote{\url{https://en.wikipedia.org/wiki/Chaos_theory}
\citep{wiki:Chaos_theory}}, 
describes chaos as: 
`When the present determines the future, but 
the approximate present 
does not approximately determine the future.` 

Dave Snowden as founder of 
the Cynefin framework, 
which contains 
Chaos and Complexity, 
defines chaos as 
`Cause and effect are unclear` 
\citep{article-kurtz-2003}.

I can summarise chaos theory 
and the Cynefin framework 
on chaos as that chaos is the 
situation where there is a lack of
information and therefore lack of predictability.
This lack of information can be compensated with
more information and/or more knowledge, 
but there is a limit to the expansion of information and knowledge.

\subsection{Exponential effect is a non-linear effect}

Sometimes small deviations 
in the input have exponential 
effect on the output. 
When there is a combination of various 
systems with this behaviour the combination of 
systems quickly exceeds the human understanding.
The weather 
prediction\footnote{\url{https://www.knmi.nl/kennis-en-datacentrum/achtergrond/over-de-weer-en-klimaatpluim-en-expertpluim}}
is an example of this.
The weather forecast in the near future 
has a higher predictability then
the weather forecast for over a few days. 
The exponential growth created 
by the non-linear complex systems 
makes it very hard to predict the weather 
in the future.

\subsection{The universe is unpredictable by nature}

There is recent research that indicates that 
chaos (unpredictability) is also part of physics 
(quantum mechanics) 
\citep{article-bera-2017}. 
This implies that there is not only a limit 
on the information and knowledge
of the observer, 
but also a root-cause in unpredictability.

\subsection{Example of a simple chaotic system}
\label{sec:pendulum}

Chaos and unpredictability are not exclusively relevant 
for large systems (weather forecasting)
or small systems (quantum mechanics). 
It is possible to create a small wooden toy 
that shows chaotic behaviour.

A double pendulum is a simple system that 
exhibits rich dynamic behaviour with a strong sensitivity 
to initial conditions 
\citep{wiki:pendulum}.
A double pendulum is constructed 
from two deterministic systems. 
An example of a double 
pendulum\footnote{\url{https://www.youtube.com/results?search_query=double+pendulum+chaos}} 
is a pole standing straight up, 
with at the end an axis connected to the end of a beam (1). 
At the end of this beam (1) another smaller beam (2) 
is connected through an axis (see figure 
\ref{fig:pendulum}).
The rotation speed of a beam (wing) can be predicted in a model. 
The connection of the two wings
results in the amplification of minimal variations in the construction and the state.

For example, a small burr on the axis that connects the two wings, or a minimal deviation of the initial energy put into the motion. 
This concave effect of minimal input results in a movement that cannot be predicted 
and therefore can be called chaotic.
Reality is more complex than a double pendulum.
The butterfly effect is a famous way of creating 
awareness of the complexity and chaos of our reality.
See figure \ref{fig:pendulum} for the double pendulum and the butterfly effect.

I personally use this example 
often when talking with other architects.
We as architects tend to forget 
the behavioural effect that can occur 
when we let various systems react on each other 
in an automated way.
An example of this is software behind the 
High Frequency 
Trading\footnote{\url{https://en.wikipedia.org/wiki/High-frequency_trading}
\citep{wiki:frequency_trading}
}
and 
Algorithmic 
Trading\footnote{\url{https://en.wikipedia.org/wiki/Algorithmic_trading}
\citep{wiki:Algorithmic_trading}}.


\begin{figure}[ht]
    \centering
    \includegraphics[width=1\linewidth]{chaos}
    \caption{Schematic view of a double pendulum and butterfly 
    effect.}
    \label{fig:pendulum}
\end{figure}

\newpage
\subsection{We are limited by observation}
\label{sec:view-model}

One of the great ideas 
in the eastern and western philosophy
is that we are observing a reflection of the reality.
In the western philosophy this is communicated 
in concepts as Plato's
cave\footnote{\url{https://plato.stanford.edu/entries/plato-myths} \citep{plato-myth}
}\textsuperscript{,}\footnote{\url{https://plato.stanford.edu/entries/plato-metaphysics}
   \citep{plato-Sun-Line-and-Cave}}
and "Dinge f\"{u}r mich" of 
Kant\footnote{\url{https://plato.stanford.edu/entries/kant}}\textsuperscript{,}\footnote{\url{https://thegreatthinkers.org/kant/introduction}}.

That reality is a reflection seen through a lens, 
implies that there is a light source 
that is casting shadows and light, 
and that there is a lens trough 
the light and shadows reach the observer.

\subsection{Light creating reflections}

The reflection in the cave of Plato is created by a fire.
In the 
Demoscene\footnote{\url{https://en.wikipedia.org/wiki/Demoscene} 
\citep{wiki:demoscene}}, 
a popular way of simulating fire 
is by generating random pixels with random 
colours\footnote{\url{https://lodev.org/cgtutor/fire.html}}. 
The concept of a random generator 
as the source of the light 
that provides reflections of our reality 
is a nice concept to think about.
Since all the events in our world are resembling random events, 
this might imply that we are all observing reality though a 
random light source.

\subsection{Our personal lens}

Where the reflection of reality (our observation) 
is limited by our personal lens,
it is also limited by the view-model we create of the world.
This has two reasons.

First the view-model is limited by the language in which we create it. 
G{\"o}del\footnote{\url{https://en.wikipedia.org/wiki/Godel's_incompleteness_theorems}
\citep{wiki:godel}
} 
proved with his incompleteness theorems 
that every language is not complete therefore limited 
\citep{book-hofstadter-2000}. 
An example of this is 
that you as a human cannot describe 
what you do not yet know, and 
that you don't know what you don't know. 
Therefore, asking someone to specify the needs delivers 
an incomplete answer since the answer is limited 
to the imagination and experience of this specific human 
\citep{MalcolmG16:online}.

The second reason that the view-model limits our view 
of the world 
is that the model often defines 
how the world is to be interpreted (translated). 
An example of this is the interface of a car lease company. 
When the IT system states that the car is outfitted 
with tires suitable for the winter,
the experience is that it is difficult to convince 
the operator of the software
that the physical car is outfitted with tires suitable 
for the summer.
The information and the logic materialised in the IT system 
defines the view on reality of the human operating 
the software. 

The view-model materialised in the IT system is limiting 
the description of the physical reality. 
This will result in situations described as 
`Computer Says No'\footnote{\url{https://en.wikipedia.org/wiki/Computer_says_no}
\citep{wiki:computersaysno}}
and in situations where actions are limited 
by the options the model (rules) provide us
\citep{book-hoogervorst-2017, book-ciborra-2004}.

\subsection{We have inherently flawed observations of the reality}

Therefore, the reflection of the world is not only limited by 
the random events that provide light (observations),
but also limited by the lenses have by experience, 
social constructs, and IT systems.