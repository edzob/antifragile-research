%!TEX root =  ../main.tex

\newpage
\section{Learning Organisation}
\label{sec:learning-organisation}

To deal with non-stop change it is essential to non-stop learn 
\citep[p. 91]{article-dietz-2013}. 
In this section I will reason that learning needs communication,
no naivety and includes enriching your organisational language
based on good and bad experiences. 
At the end I will present the selected framework 
for a learning organisation. 
\cite{article-anthony-2016} 
and 
\cite{article-dietz-2013}
showed that 
(1) adapting to the market (the environment) 
and 
(2) non-stop learning is key to survive as a company 

\begin{quote} ``This type of learning 
	acknowledges the non-planned, emerging character 
	of many enterprise developments 
	\citep{book-hoogervorst-2009}. 
	Hence, 
	\textbf{employee involvement and participation} 
	are essential for addressing 
	enterprise dynamics, complexity, and uncertainty. 
	\textbf{Enterprise change, hence redesign, 
	is thus fuelled by enterprise learning}.
	As \cite{book-weick-2001} observes, 
	\textbf{redesign is a continuous activity} 
	whereby 
	the responsibility for (re)design 
	is dispersed and rests with 
	enterprise members 
	who are coping with the ‘unexpected’.'' 
	\\ - \citep[p. 91]{article-dietz-2013}
\end{quote}

\subsection{Learning needs communication}

The above-mentioned employee involvement 
and participation needs cooperation.
Cooperation between people is made possible by communication  
\citep[p. 15]{book-janssen-2015}.

Communication is an important part 
in the design and operation of an organisation.
The definitions of fragile, robust and antifragile 
in the context of organisations pivot around 
the reaction on input 
from the outside the organisation by the organisation.

Communication is needed to learn and to adapt,
therefore communication plays a large role 
in becoming resilient and antifragile.
Communication is not only important for learning 
from the things that went good (monotonicity).
\nameref{sec:non-mono}.
Communication is therefore also related to 
non-monotonicity.

\subsection{Non-Monotonicity is learning from bad experiences}
\label{sec:non-mono}

In various papers on antifragile the term 
non-monotonicity is being used.
Monotonicity is a term used in the context of 
electrical engineering, learning and logic. 
Monotonicity can be summarised as: 
discriminate the input and only learn from improvements. 
Thus, exclude negative input from the learning process. 

Learning in the context of antifragility can be summarised as 
the increase of the variety in the language that is being used
\citep{article-ghaderi-2016, article-helbing-2013-economics, article-lange-1991, article-voorspoels-2015}.
Non-Monotonicity is thus about 
not only learning from the good experiences 
but also learning from the bad experiences
resulting in the increase of variety of the language 
in the organisation.

Learning from mistakes can be an effective defence 
against stressors. 
Mistakes and failures can lead to new information. 
As new information becomes available 
it defeats previous thinking, 
which can result in new practices and approaches 
\citep{article-augusto-2001, article-nute-2003, article-governatori-2007}.
In this way stressors cause the system to improve 
\citep{article-johnson-2013-cas, article-org-ghasemi-2017}. 

Learning from negative consequences induced 
by stressors can lead to new information. 
New information can result in improved practices 
and approaches. 
Stressors, when learned from, 
can thus cause a system to improve
\citep{article-org-kennon-2015, article-org-ghasemi-2017, article-jackson-2013}. 


\subsection{Remove naive intervention to learn}
\label{sec:naiv-intervention}

\cite{book-senge-1990}
states that reacting on what happens 
can only be successful 
if the reaction is not based on a
`linear--direct--relation between cause and effect` 
\citep[p. 14]{book-janssen-2015}. 
Taleb also supports this when talking about 
the reaction on stressors and 
the role of randomness in the context of stressors 
\citep{book-taleb-2001, book-taleb-2007,book-taleb-2012}. 
Taleb argues that systems are (almost) never linear-systems 
due to complexity, chaos and randomness, 
see also section 
\ref{def:randomness}
(\nameref{def:randomness}). 
Taleb argues to remove the  
`naive intervention` 
since this is the manifestation of reasoning linear 
where it is not applicable \citep{book-taleb-2012}.
Taleb promotes trusting on the instinct and behaviour that
individuals or group of individuals have learned over time 
above the `naive intervention` 
based on a linear and reductionistic view on the situation.

\subsection{Learning organisation is relevant for antifragility}

The learning organisation is a way to create 
a resilient organisation 
which let organisations cope with unknown 
and unpredictable events.
\cite{article-risk-aven-2015} 
provides evidence (concerning leakages in the 
oil \& gas industry) that resilient behaviour is needed to
deal with unpredictable events. 

Aven provided evidence that evolution 
from fragile to resilient is possible.
Taleb supports that data can be used to prove this. 
Fragility and antifragility 
of the same system can be measured 
since these are properties of the current object
 \citetext{\citealp{ book-taleb-2012}; 
 \citealp{article-starossek-2010}; 
 \citealp[p. 477]{article-risk-aven-2015}}.
Taleb also states that fragility of two systems can easily be compared 
\citetext{\citealp[p. 477]{article-risk-aven-2015}; 
\citealp[p. 22]{book-taleb-2012}}.

This can be combined into stating 
that an antifragile organisation 
can be the result of a fragile organisation, 
since an organisation can evolve 
from fragile to a resilient organisation
and antifragile has overlap with the 
CAS resilient (type 3) organisation.
Therefore, the learning organisation is 
of fundamental importance for an organisation 
to become resilient or even antifragile.

\subsection{How to achieve a Learning Organisation}
\label{sec:senge}

There are numerous ways to define what is needed 
in a learning organisation.
\cite{book-floor-1999} 
provides the following overview of frameworks 
for learning organisation.

\begin{enumerate}
    \item Senge’s\footnote{\url{https://en.wikipedia.org/wiki/Peter_Senge}
    \citep{wiki:peter-senge}}
    -
    5\textsuperscript{th} 
    discipline\footnote{\url{https://en.wikipedia.org/wiki/The_Fifth_Discipline}
    \citep{wiki:5th-discipline}} 
    \\\citep{book-senge-1990}
    
    \item Bertalanffy’s\footnote{\url{https://en.wikipedia.org/wiki/Ludwig_von_Bertalanffy}
    \citep{wiki:Ludwig_von_Bertalanffy}} 
    -
    Open systems 
    theory\footnote{\url{https://en.wikipedia.org/wiki/Open_system_(systems_theory)}
    \citep{wiki:Open_system}} 
    \\\citep{article-bertalanffy-1972}
    
    \item Beer’s\footnote{\url{https://en.wikipedia.org/wiki/Stafford_Beer}
    \citep{wiki:Stafford_Beer}} 
    -
    Organisational 
    cybernetics\footnote{\url{https://en.wikipedia.org/wiki/Management_cybernetics}
    \citep{wiki:Management_cybernetics}}
    \\also known as Viable Systems 
		Method\footnote{\url{https://en.wikipedia.org/wiki/Viable_system_model}
        \citep{wiki:vsm}} 
		(VSM)
        \\\citep{book-beer-1974,
		book-beer-1979, 
		book-beer-1981, 
		book-beer-1985, 
		article-beer-1989}
		
    
    \item Ackoff’s\footnote{\url{https://en.wikipedia.org/wiki/Russell_L._Ackoff}
    \citep{wiki:Ackoff}} 
    - 
    Interactive 
    planning\footnote{\url{https://en.wikipedia.org/wiki/Interactive_planning}
    \citep{wiki:Interactive_planning}}
    \\\citep{book-askoff-1981, article-ackoff-1997, article-ackoff-2001}
    
    \item Checkland’s\footnote{\url{https://en.wikipedia.org/wiki/Peter_Checkland}
    \citep{wiki:Peter_Checkland}}
    - 
    Soft systems 
    approach\footnote{\url{https://en.wikipedia.org/wiki/Soft_systems_methodology}
    \citep{wiki:Soft_systems_methodology}
    } 
    \\\citep{article-checkland-1989}
    
    \item Churchman’s\footnote{\url{https://en.wikipedia.org/wiki/C._West_Churchman}
    \citep{wiki:West_Churchman}
    }
    - 
    Critical systemic thinking
    \\\citep{book-churchman-1968, book-churchman-1979, book-churchman-1984}
\end{enumerate}


\cite{book-senge-1990} 
is used in several papers on antifragility
\citep{article-jaaron-2014, 
 article-jaaron-2014-learning, 
 article-org-kennon-2015, 
 article-nurmi-2019,
 thesis-maveau-2017,
 article-eijnatten-2004,
 article-eijnatten-2004-B}.
\cite{book-senge-1990} is also used in the context of systems approach
 \citep{article-nurmi-2019, 
 book-meadows-2008, 
 book-kok-2018, 
 book-hoogervorst-2017}.
 
This is the reason I choose to use the 
5\textsuperscript{th} discipline of Senge
as the framework to capture the system attributes from the 
selected literature 
that address the learning organisation.

\cite{book-senge-1990} identifies five disciplines 
that form together the learning organisation
\citep[p. 9-13]{book-senge-1990}.

\begin{enumerate}
    \item Personal mastery
    \item Shared mental models
    \item Building shared vision
    \item Team learning
    \item Systems thinking
\end{enumerate}