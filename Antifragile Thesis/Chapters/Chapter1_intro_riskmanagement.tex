%!TEX root =  ../main.tex

\newpage
\section{Introduction into Risk Management} 
\label{intro:where}


The survival of a system is dependent 
on the predictability 
of the course of the system 
inside its environment 
and the predictability of the environment 
so that risk can be managed 
\citep{article-se-gorgeon-2015}.
Stressors that harm a system 
are able to ultimately break a system 
\citep{book-taleb-2012, 
article-antifragile-taleb-2013, 
article-se-gorgeon-2015}.
Since risk management is about 
the identification, evaluation, and 
prioritisation of risks 
the topic of stressors, 
resilience and antifragility are relevant for risk management
\citep{book-hutchins-2018, 
article-risk-aven-2015,
wiki:risk}. 


\subsection{Risk Management - Design for survival} 

The triad (fragile, robust and antifragile) 
is relevant in the design for the survival of a system 
since fragility is, in essence, 
the sensitivity of a system 
of a given risk measure 
to an error 
\citep[p. 7]{article-antifragile-taleb-2013}.


Risk management aims to minimise, monitor, and control 
the probability or impact of unfortunate events 
or to maximise the realisation of opportunities
\citep{book-hutchins-2018,
wiki:risk}. 
This is why risk management is closely linked 
to the topics of resilience and antifragility. 

\subsection{Risk Management - Moving away from fragile and robust}

Fragility is the reason to apply risk management
in an organisation. 

Risk management is as a discipline 
moving away from the minimisation of unfortunate events 
and effects.
It is moving towards the maximisation 
of the realisation of opportunities.
The minimisation of unfortunate events 
can be found in robust, 
engineering resilience and 
systems resilience.

The implicit goal has shifted from 
designing (predictable) robust organisations 
to the need for designing 
resilience and antifragility organisations 
\citep{book-hutchins-2018}. 
This since a robust organisation requires planning
and it showed that strategic planning is not successful
\citep{book-mintzberg-1994, 
article-mintzberg-1994, 
article-risk-Derbyshire-2014, 
book-hoogervorst-2017}.

\subsection{Risk Management - Moving towards antifragility}

The maximisation of the realisation of opportunities 
can be found in Complex Adaptive Systems resilience
and antifragility. 

Risk management also `includes 
to improve the understating of risk 
by identifying signals 
and warnings 
and acknowledging uncertainties 
and the importance of knowledge' 
\citep[p. 480]{article-risk-aven-2015}. 
The definition of risk management 
is further explored in section 
\ref{def:rm}
(\nameref{def:rm}). 


\subsection{Uncertainty in a volatile and complex world}
\label{sec:rm-vuca}

Most, if not all, 
organisations operate in a context 
which is described as 
Volatile, Uncertain, Complex and Ambiguous (VUCA) 
\citep{article-bennet-2014-bhr, 
article-bennet-2014-bh, 
book-mack-2015, 
book-hutchins-2018,
wiki:vuca}.  
\begin{quote}
    ``We live in a ... VUCA time. ... 
    In terms of 
    ISO 31000:2018\footnote{\url{https://en.wikipedia.org/wiki/ISO_31000} 
    \citep{wiki:ISO_31000}} 
    and 
    ISO 9001:2015\footnote{\url{https://en.wikipedia.org/wiki/ISO_9000}
    \citep{wiki:ISO_9000}}, 
    the concept of 'uncertainty' is integrated 
    throughout the standards.`` 
    \\- \citep[chapter 1]{book-hutchins-2018}
\end{quote}
\begin{quote}
    ``We’re entering a period of heightened volatility for 
    leading companies across a range of industries, 
    with the next ten years shaping up 
    to be the most potentially turbulent in modern history.``
    \\- \citep[p. 3]{article-anthony-2016}.
\end{quote}

The uncertainties that 
\cite[p. 480]{article-risk-aven-2015} names 
are part of a larger domain of stressors 
which is called the `disorder cluster' 
also known as VUCA 
\citetext{\citealp[p. 436]{book-taleb-2012}; 
\citealp[p. 3]{article-se-gorgeon-2015}}.


Black swans (or X-events) are defined as 
stressor events in the VUCA domain 
that have a disruptive impact 
on a fragile and robust system and are non-predictable
\citetext{\citealp{book-taleb-2007}; 
\citealp{article-bennett-2008};
\citealp[p. 18]{book-taleb-2012}; 
\citealp{book-casti-2012}; 
\citealp{article-johnson-2013-cas}; 
\citealp{article-antifragile-taleb-2013}
\citealp[p. 479]{article-risk-aven-2015}; 
\citealp[p. 14]{book-hole-2016}; 
\citealp[p. 23]{article-org-ghasemi-2017}}.

\subsection{Antifragility is mandatory to stay relevant in a VUCA world}

By learning from the past and 
by being a resilient organisation 
it is possible to extend the period 
of being relevant as an organisation.

\begin{quote}
    ``After all, we can’t know for sure what the future holds. 
    But companies can adapt to change 
    by launching new growth ventures 
    that move them beyond their historic core business. 
    Our work shows that the companies that 
    continually find ways to reinvent themselves 
    are the ones that control their own destiny.``
    \\- \citep[p. 8]{article-anthony-2016}.
\end{quote}

Taleb reasons that 
an antifragile system is mandatory 
to survive a black swan event 
and therefore is mandatory to stay relevant 
in a VUCA world \citep{book-taleb-2012}.
Antifragile systems have a positive sensitivity for disorder 
\citep[p. 1]{article-antifragile-taleb-2013}.

\subsection{Antifragility provides design guidelines for Risk Management}

An enterprise is an intentionally created 
Complex Adaptive System (CAS) 
consisting of cooperating human beings  
with a certain social purpose,
whereby it is impossible to determine the ultimate
(operational) reality of the enterprise 
down to the minute details
\citep[p. 93-94]{article-dietz-2013}. 
A CAS\footnote{\url{https://en.wikipedia.org/wiki/Complex_adaptive_system}, 
\citep{wiki:cas}} 
is a type of a non-linear dynamical system in 
the field of Complexity Science and Systems Engineering 
\citep{article-lansing-2003}.

In the scope of organisations, 
antifragile plays a role in the domain of 
organisation design and in the sub-domain of risk management 
\citep{article-risk-aven-2011, 
article-risk-aven-2012, 
article-scholz-2012, 
article-risk-aven-2015, 
book-hutchins-2018}.

\subsection{Organisations, Resilience, Antifragile and System-of-Systems}

Sometimes I wonder if it is better to rephrase 
`antifragile is the antithesis of fragile`
into 
`antifragile behaviour is the antithesis of 
fragile behaviour of a system`. 
Since an antifragile system needs to have 
a fragile part of the system that
creates the hurt that enables 
the system as a whole to react on to get stronger.

For example, in the case of our immune system. 
First cells need to die and the host needs
to get (a little bit) sick to develop immunity to the disease.

\subsubsection{An antifragile system needs to have a fragile sub-system}

It also can be reasoned that when 
there is a fragile sub-system, there also needs
to be a robust sub-system. When the body of Hydra dies when 
one head is chopped off, 
then Hydra as a whole is as fragile as the head.
The same for a body that is getting sick. 
When the disease leads to immediate death
then the body as a whole is fragile. 

\subsubsection{An antifragile system needs to have a robust sub-system}

\citet{article-martin-breen-2011} 
state that a system in the scope of 
resilience 
is seen as a System-of-Systems.
\citet{book-hutchins-2018} 
states that a system in the scope of 
Risk Management 
is seen as a System-of-Systems.
\citet{article-dietz-2013} 
state that an organisation is a Complex-Adaptive-System.
A Complex-Adaptive-System is a System-of-Systems.




