%!TEX root =  ../main.tex
\newpage
\section{Literature review}
\label{bok}

\subsection{Literature review approach}

For the creation of an overview of the current Body of Knowledge,
that is relevant to Antifragility, 
I used the divergent and convergent
process (see figure \ref{fig:diverge-converge}). 
To diverge  
is in this context to move in different directions 
from a common point 
creating choices by including more and more in the collection
\citep{diverge18:online}.
To converge 
is in this context to move towards one another 
by making choices that
determine if something is in scope or out of scope
\citep{converge18:online}.
Both my divergent and convergent processes were iterative. 
Iterations where triggered by the 
`understanding relationships` step
(figure 
\ref{fig:model-theoretical_framework}).

\begin{figure}[ht]
\centering
    \includegraphics[width=0.6\linewidth]{diverge-converge}
    \caption{Diverge and Converge process.}
    \label{fig:diverge-converge}
\end{figure}

My approach is inspired by the snowball\footnote{\url{https://libguides.rug.nl/c.php?g=470628&p=3218096}} 
method 
\citep{article-wohlin-2014, Searchme67:online}
as part of a 
systematic\footnote{\url{https://en.wikipedia.org/wiki/Systematic_review}} 
literature review
\citep{wiki:Systematic_review}.
I retrieved relevant literature 
from three groups of sources: 
`primary sources` 
for literature from the source material, 
`secondary sources` 
for literature from academic search engines and 
an `extension on the secondary sources` 
for literature from book, thesis and dissertation search engines.
See section \ref{sec:primary-sources}.

\subsubsection{Size of the snowball}
The primary sources are the start 
of the divergence step 
in my literature research.
This resulted in: 
\begin{itemize}
    \item 358 sources cited in this thesis 
    (\hyperref[sec:bibliography]{Bibliography}).

    \item 87 sources categorised  
    (Appendix \ref{Appendix:antifragile-literature}).

    \item 9 sources selected as source for the attributes of the EAAL model 
(section \ref{sec:attributes-used}).
\end{itemize}


\newpage 
\subsection{Creating the overview of the Body of Knowledge}

\subsubsection{Primary sources}
\label{sec:primary-sources}

As `primary sources` for the literature 
I used references from the book 
Antifragile 
\citep{book-taleb-2012}, 
and the references from the Wikipedia pages on 
Antifragile 
\citep{wiki:Antifragile} 
and 
Antifragility 
\citep{wiki:Antifragility}.

\subsubsection{Secondary sources}
\label{sec:secondary-sources}

In addition to the literature found in 
the `primary sources` 
I added literature found via various
academic search engines: 
I used 
(1) Google Scholar\footnote{\url{https://scholar.google.com}}, 
(2) Bing Academic\footnote{\url{https://www.bing.com/academic}},
(3) Semantic Scholar\footnote{\url{https://www.semanticscholar.org}},  
(4) ReseachGate\footnote{\url{https://www.researchgate.net}},
(5) Citationsy\footnote{\url{https://citationsy.com}} and the 
(6) Library of 
Antwerp Management School\footnote{\url{https://vpn1.uantwerpen.be/+CSCOE+/logon.html}}.

\subsubsection{Extension on the secondary sources}
\label{sec:secondary-ext-sources}

I used the following search engines for finding books: 
(1) Amazon.de\footnote{\url{https://www.amazon.de/}},
(2) Goodreads\footnote{\url{https://www.goodreads.com/}}
and
(3) Google Books\footnote{\url{https://books.google.com/}}.
Via this way I found the thesis of 
Dennis Kastner \citep{thesis-kastner-2017}.

The thesis of 
\cite{thesis-kastner-2017}
and 
\cite{thesis-henriksson-2016}
inspired me to search in public thesis databases: 
(4) Diva\footnote{\url{http://www.diva-portal.org/}} 
(Sweden),
(5) Scripties Online\footnote{\url{http://scripties.ned.ub.rug.nl/}} 
(the Netherlands),
(6) Narcis\footnote{\url{https://www.narcis.nl/}}
(the Netherlands),
(7) OpenThesis.org\footnote{\url{http://www.openthesis.org/}}
(USA),
(8) OATD\footnote{\url{https://oatd.org/}}
(Global)
and 
(9) Sci-Hub\footnote{\url{https://sci-hub.tw/}}
(Global)
.

\subsubsection{Search keywords}

I used the following keywords during my initial searches:\\
`antifragile`;
`anti-fragile`; \\
`antifragility`;
`anti-fragility`; \\
`Taleb`;
`Nassim Taleb`;\\
`antifragile organisations`;
`antifragile organizations`;\\
`anti-fragile organisations`;
`anti-fragile organizations`.
\\After using these keywords my research queries became more organic and iterative based on the literature found in the reference lists of the found literature.

\subsubsection{Body of Knowledge is made openly available}

An overview of the current (2019) Body of Knowledge 
on the topic of antifragile is presented in
Appendix \ref{Appendix:AF-papers} 
(\nameref{Appendix:AF-papers})
and also available via the
wiki\footnote{\url{https://gitlab.com/edzob/antifragile-research/wikis/Antifragility-Literature-Research}} 
of this public research project on Gitlab.

\subsubsection{Reference of this thesis is made openly available}

The reference list of this thesis is a sub-set of the collected Body of Knowledge.

The reference list created during this research is available via the
BiBTeX\footnote{\url{https://gitlab.com/edzob/antifragile-research/-/tree/master/Antifragile Thesis}} 
files of this research project. 

\newpage
\subsection{Keywords relevant to antifragile}

Part of the convergence process is understanding 
the relationship between the theories.
I created a list of the identified 
relevant scientific keywords. 
These keywords can be used by everyone 
that is researching antifragility or
for who that is aiming to replicate my research.

\subsubsection{Keywords - Systems and Complexity Theories}

Complexity Science\footnote{\url{https://en.wikipedia.org/wiki/Complex_system}},
Emergence\footnote{\url{https://en.wikipedia.org/wiki/Emergentism}},
Self-organisation\footnote{\url{https://en.wikipedia.org/wiki/Self-organisation}},
Collective behaviour\footnote{\url{https://en.wikipedia.org/wiki/Collective_behavior}}, 
Network Science\footnote{\url{https://en.wikipedia.org/wiki/Network_science}}, 
Evolution\footnote{\url{https://en.wikipedia.org/wiki/Evolution}} 
and adaptation\footnote{\url{https://en.wikipedia.org/wiki/Adaptation}}, 
Game Theory\footnote{\url{https://en.wikipedia.org/wiki/Game_theory}}, 
non-Linear Dynamic Systems\footnote{\url{https://en.wikipedia.org/wiki/Dynamical_system}}, 
Systems Theory\footnote{\url{https://en.wikipedia.org/wiki/Systems_theory}}, 
Systems Engineering\footnote{\url{https://en.wikipedia.org/wiki/Systems_engineering}}, 
Systems of Systems\footnote{\url{https://en.wikipedia.org/wiki/System_of_systems}}, 
Complex Adaptive Systems (CAS)\footnote{\url{https://en.wikipedia.org/wiki/Complex_adaptive_system}}, 
Complexity Management\footnote{\url{https://en.wikipedia.org/wiki/Complexity_management}}, 
Chaos Engineering\footnote{\url{https://en.wikipedia.org/wiki/Chaos_engineering}}, 
Entropy\footnote{\url{https://en.wikipedia.org/wiki/Entropy}}, 
Distributed Systems\footnote{\url{https://en.wikipedia.org/wiki/Distributed_computing}}, 
Site Reliability Engineering\footnote{\url{https://en.wikipedia.org/wiki/Site_Reliability_Engineering}}

\subsubsection{Keywords - Organisation Design}

Risk Management\footnote{\url{https://en.wikipedia.org/wiki/ISO_31000}}, 
Barbell Strategy\footnote{\url{https://en.wikipedia.org/wiki/Barbell_strategy}}, 
Requisite Variety\footnote{\url{https://en.wikipedia.org/wiki/Variety_(cybernetics)}}, 
Emergence\footnote{\url{https://en.wikipedia.org/wiki/Emergence}}, 
Holacracy\footnote{\url{https://en.wikipedia.org/wiki/Holacracy}}, 
Taylorism (Scientific Management)\footnote{\url{https://en.wikipedia.org/wiki/Scientific_management}}, 
Enterprise Engineering\footnote{\url{https://en.wikipedia.org/wiki/Enterprise_engineering}}, 
Enterprise Architecture\footnote{\url{https://en.wikipedia.org/wiki/Enterprise_architecture}}, 
Enterprise Design\footnote{\url{https://en.wikipedia.org/wiki/Organizational_architecture}}, 
Enterprise Governance\footnote{\url{https://en.wikipedia.org/wiki/Corporate_governance}}, 
Employee Empowerment\footnote{\url{https://en.wikipedia.org/wiki/Empowerment}}, 
Autonomy\footnote{\url{https://en.wikipedia.org/wiki/Autonomy}}, 
Resilience\footnote{\url{https://en.wikipedia.org/wiki/Business_continuity_planning}}, 
Decoupling\footnote{\url{https://en.wikipedia.org/wiki/Decoupling_(organisational_studies)}}, 
Knowledge Management\footnote{\url{https://en.wikipedia.org/wiki/Knowledge_management}}

\newpage
\subsection{Categories to organise the Body of Knowledge}

Based on the relationships I recognised in the collected literature,
I determined the following six categories 
to label and sort the found literature:
(1) `Antifragile`; 
(2) `Antifragile \& IT`;
(3) `Organisation`; 
(4) `Risk and Resilience`;
(5) `Complexity Science`;
(6) `Science`.

For `Organisation` 
I recognised papers that
focus on organisations, 
how they respond to stress and the role of the human factor.

For `Risk and Resilience` 
I recognised papers that,
focus on risk, robust, fragile, resilience, emergence, 
entropy, chaos, unpredictability and/or stress.

For `Science` 
I recognised papers that,
focus on science and how it is limited by observation.
I based my approach of categorisation on 
\cite{book-wolcott-1990}.
See Appendix 
\ref{Appendix:AF-papers} 
(\nameref{Appendix:AF-papers})
for the sorted list of the papers found.
