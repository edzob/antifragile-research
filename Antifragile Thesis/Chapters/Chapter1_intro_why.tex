%!TEX root =  ../main.tex

\newpage
\section{Why a thesis about antifragility?}
\label{sec:quick-intro}
\label{sec:why}

Taleb introduced the term antifragile 
as the concept of gaining from disorder  
\citep{book-taleb-2012}. 
His publication of the concept of antifragile sparked 
the creation of many blog articles and books on this
topic\footnote{\url{https://gitlab.com/edzob/antifragile-research/wikis/Antifragility-Literature-Research}}. 

I present in this thesis a model of 
system attributes that are relevant for being
resilient or to become antifragile. 
The list of attributes is limited 
to what is available in the existing Body of Knowledge. 
This model is an Extended Antifragile Attribute List (EAAL).
The EEAL model is a synthesis (summary) 
of the available Body of Knowledge to date (2019). 
I do not prove nor do I claim 
that these attributes will lead 
to the specified type of resilience.
The EAAL model is just a little step 
in bringing people together 
towards a shared mental model in this 
very interesting a new field of science.

Nassim Nicholas Taleb wrote down his journey 
towards antifragility 
in three books in the period 2001-2012.
These books are titled:
"Fooled by Randomness", 
"The Black Swan" and 
"Antifragile" 
(fig. \ref{fig:taleb-books}) 
\citep{book-taleb-2001, book-taleb-2007, book-taleb-2012}.

By coincidence I read these books each 
in the year they were published 
and they have had a big impact 
on my personal journey into science 
and into the topic of a shared model of reality 
(Appendix \ref{Appendix:personal-motivation}, 
\nameref{Appendix:personal-motivation}). 
This also motivates me highly 
to write my thesis on the topic of antifragility.


\begin{figure}[ht]
\centering
    \includegraphics[width=1\columnwidth]{taleb-books}
    \caption{Fooled by Randomness, Black Swan and Antifragile by Nassim Nicholas Taleb.
    Images retrieved from \href{https://www.goodreads.com}{Goodreads.com}.}
    \label{fig:taleb-books}
\end{figure}

\subsection{Fooled by Randomness, Taleb 2001}
In "Fooled by Randomness" Taleb tells us that reality is more a collection
of random events then the logical outcome of a predictable chain of events 
\citep{book-taleb-2001}. 

That it is difficult to predict the future
and that most things just happen is something that 
has been stated by many other writers 
\citetext{\citealp{article-tversky-1974}; 
    \citealp{book-mintzberg-1994}; 
    \citealp{article-augusto-2001}; 
    \citealp{article-governatori-2007}; 
    \citealp{article-makridakis-2009-forecasting};  
    \citealp{article-makridakis-2009-living}; 
    \citealp{article-makridakis-2009-decision}; 
    \citealp{article-dietz-2013}; 
    \citealp{book-O'Neil-2016}; 
    \citealp{article-dietz-2017-fi}; 
    \citealp{book-hoogervorst-2017}}.

See also sections 
\ref{intro:where}
(\nameref{intro:where}),
\ref{def:randomness}
(\nameref{def:randomness}),
\ref{def:chaos}
(\nameref{def:chaos}),
\ref{def:vuca}
(\nameref{def:vuca}). 
and
\ref{sec:taleb-ee}
(\nameref{sec:taleb-ee}).

The conclusion of Taleb is: if we capture reality 
in a simplified representation (a model), 
we cannot use this model to predict what is going to happen; 
We tend to overestimate causality and 
view the world as more explainable than it really is.

\newpage
\subsection{The Black Swan, Taleb 2007}

The story continues with his book "The Black Swan" \citep{book-taleb-2007}. 
Here  Taleb states that every model that we create is limited by our observations. 
Based on these (limited) observations we tend to translate 
the outcome of correlation analyses into a model that we use as a model that states causality 
\citep{book-taleb-2007}.
The limitations of the usage of models are not new and argued by many other experts 
\citep{book-hoogervorst-2017, book-ciborra-2004}.
See also section \ref{sec:view-model}
(\nameref{sec:view-model}).

\subsubsection{Models are fragile}
Taleb combines in his book "The Black Swan" 
the content of his previous book "Fooled by Randomness"
in concluding that it is (almost) impossible 
to create a model that contains causal relations.
It is impossible to predict 
what is going to happen not only on the fact that the
information obtained through observation is never complete (1), 
but also since the behaviour in itself cannot be predicted (2). 

An example of this unpredictability 
is that the combination of two deterministic systems 
can lead to a non-deterministic system.
See also section \ref{sec:pendulum}
(\nameref{sec:pendulum}). 

The result of these two factors is that 
the model of reality, 
for example, a financial model, 
will only function in the context of 
very strict (theoretical) conditions. 
Since reality will create events 
that were not taken into account
during the creation and testing of the model, 
the model is fragile to reality. 

Something that is not foreseen 
in the design of a model 
and that will have a disruptive effect 
is called a 'black swan' \citep{book-taleb-2007}. 


\subsection{Antifragile, Taleb 2012}

Taleb did not provide a solution to the black swan problem 
in his book "The Black Swan" (2007). 
In 2012 Taleb states that the only way
for a System-of-Systems  
to survive a black swan event 
is by being antifragile
\citep{book-taleb-2012}.

Antifragile is the word he introduced as the opposite of fragile
and is in therefore in the literature also written as
anti-fragile.
See also section \ref{intro:antifragile}
(\nameref{intro:antifragile}).

\subsubsection{Taleb's definition
 of antifragile is difficult to implement}

In the book "Antifragile" Taleb provides 
a few principles (or attributes)
that are mandatory for a system to be antifragile.
Taleb provides per principle many examples, 
but to change a system a comprehensive set of principles
that have a clear relation to each other is needed. 
This is not provided. 

These principles have proven to be difficult to translate 
into action within in a specific domain.
An example is the principle of "no naive intervention".
This is a statement that inspires many but 
is not easily translated into 
the design of a business or IT solution. 



\subsection{The relevance of antifragility to the Master of Enterprise Architecture and Engineering}

When I was introduced into Enterprise Engineering (EE) 
I immediately noticed the overlap
with the way-of-thought of Taleb. 
The first overlap is the idea that models 
based on observations do not provide insight 
in the reason why things happen.

Second, both EE and Antifragility state that   
providing employees with more freedom 
makes an organisation more resilient against the stress events
\citep{article-hoogervorst-2015-sigma}.

See also section 
\ref{sec:organisation-design}
(\nameref{sec:organisation-design}), 
\ref{def:variety}
(\nameref{def:variety}),
\ref{def:stressor}
(\nameref{def:stressor})
and
\ref{ref:replication}
(\nameref{ref:replication}).

Third, there is an overlap with the discipline of 
Enterprise Architecture (EA, section \ref{def:ea}) \& 
Enterprise Engineering (section \ref{def:ee}) on design.
EA and EA concern the design of organisations and define 
organisations as a System-of-Systems.
Antifragility covers the design of System-of-Systems.
Therefore, Antifragility is relevant to 
the discipline of Enterprise Architecture and 
Enterprise Engineering. 

\subsection{The goal(s) of this thesis}
\label{sec:goal-thesis}

I have the following three goals with this thesis.
\begin{enumerate}
    \item Provide, in the form of a model, a summary of the attributes that a system should have,  
    when it wants to evolve from a robust to an antifragile system. \\
    \\The limitation here is that this model is purely based on the available literature on antifragility. 
    
    \item Provide validation of the model by experts to prove 
    that there are no inconsistencies or red-flags in the model.\\ 
    \\The limitation here is that this model does not intend to be complete since this is impossible. 
    My aim is that this model can function as a shared mental model for antifragile experts.
    Therefore, the experts consulted include various experts.
    
    One of the experts provided a lecture on Normalised Organisations at the Antwerp Management School and is daily involved in implementing Holacracy at 
    various clients.
    
    Another one of the experts is 
    someone who daily works with and educates 
    in designing antifragile IT systems.
    He started in January 2020 his PhD 
    in Software Engineering and complex systems 
    at the Open University (UK).
    He has also written papers on antifragile software 
    design \citep{article-oreilly-2019, article-oreilly-2020}. 
    
    \item Provide validation of the possibility 
    to apply this model to an organisation. \\
    \\The limitation of this validation is 
    that experiments with antifragile organisations 
    that need to be set up from scratch
    are too complex and time-consuming to be part of this thesis.
    This validation of application is done 
    with various business leaders.
    The goal is not to prove that these attributes 
    will lead to a more resilient and antifragile organisation. 
    The goal is a bit humbler. 
    The validation by the business leadership 
    only states that the language and way-of-thought 
    of the model make sense to them and 
    that they think that they can translate 
    the model into actions. 
\end{enumerate}
