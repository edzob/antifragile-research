%!TEX root =  ../main.tex

\newpage
\section{The attributes of a resilient and antifragile System-of-Systems} 
\label{sec:antifragile-resilient-sos-attributes}

\subsection{Engineering Resilience attributes}

In the papers the attribute that I grouped into 
Engineering Resilience is `balancing constraints vs freedom` 
\citep{article-johnson-2013-cas, article-org-kennon-2015, thesis-henriksson-2016}.
The selected literature addressed the attributes 
"Top-down Command \& Control" and "Micro-Management".

\subsubsection{Top-down C\&C}

Top-Down command and control in an organisation 
is that a employee is not free to decide to go left or right 
but has to follow orders. 
The careful design of an iPhone or a good pen 
is also an example of limited freedom of movement in the product itself.

\subsubsection{Micro-Management}

Micro-management is about the freedom in the use of the product. 
When there are detailed working instructions available 
in a business process, 
the employee has no freedom in the execution of the job. 
Another great example is a Lego building block. 
It is engineered and fabricated 
into the greatest detail 
creating a building block that 
is almost completely robust. 
Lego has a very small resilience behaviour through engineering. 

\subsection{Systems Resilience attributes}

\subsubsection{Redundancy}
Redundancy is about having not a single point of failure 
by making use of duplication. 
An example is a backup electricity generator. 
Another example is local government as backup system 
of the central government.

Redundancy is the duplication of critical components or 
functions of a system with the intention of increasing reliability 
of the system, usually in the form of a backup or fail-safe, 
or to improve actual system performance 
\citep{wiki:Redundancy}. 

Duplication 
of components to meet the same objective create excess capacity 
in a system and are effective tools for extreme stressor defences. 
Redundancy tends to stabilise systems and improve robustness
\citep{article-org-kennon-2015}.

The presence of more than one component to cope with failure.
Redundancy may also have the perverse effect of 
increasing complexity, raising the likelihood of failure 
\citep{book-perrow-1984}.

\subsubsection{Modularity}

Modularity is the degree that components may be separated 
and recombined, often with the benefit of flexibility
\citep{book-hole-2016, article-oreilly-2019, article-santos-2012, article-se-gorgeon-2015, article-martin-breen-2011, article-liu-2002}.
For example the finance team and the marketing team. Another example is the user-interface module and the data storage module.

\subsubsection{Loosely coupled}

Loosely coupled is the degree of dependency on the exact working 
of another module. 
For example, when the colour-schema of a website 
is changed it is preferred that this does not impact 
the functioning of the website. 
Another example is that when there are new employees 
introduced at the finance department this should not impact 
the taste of the coffee. 
It is important to understand that there is always some degree 
of coupling.

Loosely coupled is also known as 
`Weak links`, `uncoupling`, `loose / tight coupling`
and `a low level of interconnectedness between components`
\citep{book-hole-2016, article-oreilly-2019, article-org-ghasemi-2017, article-johnson-2013-cas}.


\subsection{Complex Adaptive System Resilience attributes}

\subsubsection{Diversity}
\label{sec:diversity}

Diversity is also known as optionality, 
the ability to solve a problem in more than one way with different components
\citetext{\citealp[p. xiii, 38]{book-hole-2016}; 
\citealp[p. 9, 17, 32, 33]{article-martin-breen-2011}; 
\citealp[p. 5,9]{article-se-gorgeon-2015}; 
\citealp{thesis-kastner-2017}; 
\citealp[p. 886,888]{article-oreilly-2019}}.

Diversity is used in various forms. 
For example, the usage of different programming languages 
or different programming patterns/solutions. 
This way the collection of systems is more diverse. 
An example of this is the company Booking.com 
that runs in production various A/B tests 
with a wide variety of features 
\citep[p. 1]{article-kaufman-2017}. 
Another example is that within a team you want 
diverse co-workers since other type of people come up 
with other type of solutions.

\cite{book-taleb-2012} and 
\cite{article-se-gorgeon-2015} 
both use the term optionality. 
This is a more specific form of diversity. 
Optionality is the availability of options
\citetext{\citealp[p. 176-177]{book-taleb-2012}; 
\citealp[p. 9]{article-se-gorgeon-2015}}.

For example, retrieving weather information from 
two different API end-points 
and not from one weather information supplier. 
Another example is having four delivery companies that 
transport goods from your warehouse to your customers 
instead of having only contracted one delivery company.
\cite{thesis-henriksson-2016} 
is using the term Networks with the same intention. 
The network is there to have opportunities 
in the context of having options. 
The combination of Emergence, innovation and networks 
support that diversity can be used as common attribute.

For the EAAL model I have made the design decision 
to exclude optionality from the antifragile group 
and put it in the CAS resilience group under diversity, 
since the most of the impact is in 
creating diversity.

\textbf{Additional work on optionality}
\\
In the context of CAS the term diversity is commonly used.
Interesting is that optionality 
as term is used by Taleb, Martinetti and Derbyshire.
Optionality can be seen as an extension of flexibility
\citetext{\citealp[p. 237]{article-martinetti-2017};
\citealp[p. 839]{article-florio-2014};
\citealp[p. 31,32]{article-holling-1996};
\citealp[p. 7]{book-janssen-2015}}.

\begin{quote}
    ``Optionality is seen as both building flexibility 
    to increase the range of strategic options
    as well as an enabler for convexity to respond to unpredictable events`` 
\\- \citep[p. 219] {article-risk-Derbyshire-2014}.

\end{quote}
    
\subsubsection{Non-Monotonicity}

See also section \ref{sec:non-mono} (\nameref{sec:non-mono}).

Learning from mistakes can be an effective defence 
against stressors. 
Mistakes and failures can lead to new information. 
As new information becomes available it defeats previous thinking, 
which can result in new practices and approaches.



In this case, stressors can actually cause the system to improve
\citep{article-johnson-2013-cas, article-org-ghasemi-2017}. 
Learning from negative consequences induced by 
stressors can lead to new information. 
New information can result in improved practices and approaches. 
Stressors, when learned from, can thus cause a system to improve
\citep{article-org-kennon-2015, article-org-ghasemi-2017}. 

In \cite{book-hole-2016} 
this is described as `Fail Fast` and in 
\cite{article-se-gorgeon-2015} 
as `inject randomness into the system`.

\subsubsection{Emergence}

When there is little or no traceable relation
between the `micro and macro level output`
then emergence is there.
This the situation where random things (`unintended states`) appear
more often and X-Events (black swans) appear.
The law or Requisite Variety applied in this reasoning,
leads to that  
internal emergence counters external emergence, 
and this leads then to antifragility
\citep{article-christen-2002, article-org-kennon-2015, article-org-ghasemi-2017, article-Goldstein-1999, article-menzies-1988, article-johnson-2013-cas}.

\subsubsection{Self-Organisation}

Self-Organisation is a process 
where some form of overall order arises 
from local interactions between parts 
of an initially disordered system
\citep{wiki:Self-organization}. 
For example, students sitting together in the school cafeteria.

\subsubsection{Insert low-level stress}

Continuous Improvement is achieved by inserting low-level of stress 
continuously into a learning system. 
This will keep the system sharp all the time.

\subsubsection{Network-connections}

A network is created by connections to other nodes. 
More connections increase the potential for optionality 
for new constructions and also new functionalities.

\subsubsection{Fail Fast}

The other attributes in this group combined enables 
the possibility to execute the strategy
“Fail Fast” 
\citep{article-org-kennon-2015, article-se-gorgeon-2015, book-hole-2016, article-org-ghasemi-2017}.

\subsection{Antifragile attributes.}

\subsubsection{Resources to invest}

Opportunities can only be seized when 
there are resources free to do see. 
This can be money but also time and labour. 
To survive a black swan investment should be possible 
\citep{book-taleb-2012, article-se-gorgeon-2015, thesis-kastner-2017, thesis-henriksson-2016}.

\subsubsection{Seneca’s barbell}

To be antifragile you need a robust sub-system 
to which 80-90\% predictable value 
with low risk is situated. 
The 10-20\% should be used for high return on investment activities
\citep{book-taleb-2012, article-johnson-2013-cas, article-org-kennon-2015, thesis-henriksson-2016}.


\subsubsection{Insert randomness}

When insert-low-level stress 
and fail fails delivers no issues the next step 
is to insert randomness into the systems. 
A great example of this is Chaos Engineering 
by Netflix or the HackerOne bug-bounty system
\citep{book-taleb-2012, article-org-kennon-2015, article-se-gorgeon-2015, article-org-ghasemi-2017}.


\subsubsection{Reduce naive intervention}

Naive intervention is an intervention based 
on a model and reductionistic logic 
and ignoring the experience. 
An example is not listening to the experienced 
but not so articulate employee, 
or by ignoring the balance nature has found in an ecosystem
\citep{book-taleb-2012, article-se-gorgeon-2015, thesis-kastner-2017}.

\subsubsection{Skin in the game}
\label{sec:skin-in-the-game}

Make certain that the one making the decision 
and doing the work has a pain and gain relation with the outcome. 
This goes beyond having a feedback system in place. 
This goes beyond having KPI’s in place. 
An example is that when working Agile scrum, 
the product owner should be a co-worker in the team 
for whom the solution is being build
\citep{book-taleb-2012, thesis-kastner-2017}. 

\subsection{Learning Organisation attributes}

\subsubsection{Personal mastery}

\begin{quote}
"Personal mastery is a discipline of 
continually clarifying and deepening our personal vision, 
of focusing our energies, 
of developing patience, and of seeing reality objectively."
\\- \citep[p. 7]{book-senge-1990}
\end{quote}

\subsubsection{Shared mental models}

\begin{quote}
"Mental models are deeply ingrained assumptions, 
generalizations, or even pictures of images that 
influence how we understand the world and how we take action."
\\- \citep[p. 8]{book-senge-1990}
\end{quote}

\subsubsection{Building shared vision}

\begin{quote}
"Building shared vision - a practice of 
unearthing shared pictures of the future that 
foster genuine commitment and enrolment rather than compliance."
\\- \citep[p. 9]{book-senge-1990}
\end{quote}

\subsubsection{Team learning}

\begin{quote}
"Team learning starts with 'dialogue', 
the capacity of members of a team to 
suspend assumptions and enter into genuine 'thinking together'."
\\- \citep[p. 10]{book-senge-1990}
\end{quote}

\subsubsection{Systems thinking}

\begin{quote}
"Systems thinking - The Fifth Discipline that integrates the other four.
\ldots 
Systems thinking also needs the disciplines of building 
shared vision, mental models, team learning, 
and personal mastery to realize its potential. 
Building shared vision fosters a commitment to the long term. 
Mental models focus on the openness needed to unearth shortcomings 
in our present ways of seeing the world. 
Team learning develops the skills of groups of people to look 
for the larger picture beyond individual perspectives. 
And personal mastery fosters the personal motivation 
to continually learn how our actions affect our world."
\\- \citep[p. 12]{book-senge-1990}
\end{quote}
