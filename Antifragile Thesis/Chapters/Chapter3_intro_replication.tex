%!TEX root =  ../main.tex

\newpage
\section{The replication challenge}
\label{ref:replication}

The following section describes the replication challenge.
This topic is relevant for the topics of antifragility and 
risk management. It also is relevant for the scope of 
my research questions and the methodology applies.

\subsection{Antifragile and scientific models created by observations}

In the scientific world there is a discussion
that overlaps with the point of view
of Taleb that he describes in his books 
Black Swan 
\citep{book-taleb-2007} and 
Antifragile 
\citep{book-taleb-2012}. 
In this section an introduction into this field, that can be summarised 
to the statement that models cannot be used to predict and replicate outcome.

\subsection{Taleb and Enterprise Engineering}
\label{sec:taleb-ee}

Taleb states that 
every model created by humans 
based on observations
is faulty on a fundamental level 
\citep{book-taleb-2001, book-taleb-2007, book-taleb-2012}.
This is a premise that is also fundamental to the field of Enterprise Engineering 
\citep{article-dietz-2013, article-dietz-2017-fi}. 
The reasoning of Enterprise Engineering. 
Enterprise Engineering reasons 
that observation is always subjective 
and therefore representing a single instance 
of a personal view on reality 
\citep{book-hoogervorst-2017}. 
Enterprise Engineering and the reasoning of Taleb are complementary to each other.

\subsection{Rethinking Economics}
In the scientific economical world, there is the movement called "\textit{Rethinking Economics}". This movement started in 1991 \citep{book-hodgson-1991, article-helbing-2013-economics}
This movement states that all economical models 
that are being used are based on measurements (quantitative science) 
and models based on these measurements and that this method is flawed. 
The economic models are flawed because this approach lacks the involvement 
of the human factor and the notion that not everything is rational
and can be explained by reductionism. 
The economic crisis of 2008 is one of the big arguments of this movement to include qualitative (empirical) research into the field.

\subsection{Replication Crisis}
In the field of psychology there is the movement called "\textit{the replication crisis}"
\citetext{\citealp{stuk-rood-vlees-7}; 
    \citealp{YANSS-100};
    \citealp{Egodeple49:online}}.
Models in the more social sciences are based on qualitative (empirical) studies. 
In this area of science, the human factor is crucial in explaining why things happen. 
Scientists tried to reproduce one of the pinnacle studies in the field of psychology called the Ego experiment. 
Initial the replication of this study did not success and when over 60 different group of researchers failed in replicating this research the "replication crisis" became relevant for the whole field of social studies \citep{article-hagger-2016}.
This replication crisis is older and more generic then only the ego depletion study \citep{article-Ioannidis-2005}.
The centre for open science facilitated a project with 270 scientists that sought to replicate 100 different studies published in 2008. In the end two-thirds failed to replicate
\citetext{\citealp{OSFParti94}; 
    \citealp{article-open-reproducibility-2015}}. 


\subsection{Black swans and scientific models}

Taleb argues in Black Swan (2007) that quantitative research only states about the data in the data-set that is collected and states nothing about the reality. 
Taleb also argues in Antifragile (2012) that qualitative research is very tricky since it states something on the current context and the current context is non-stop changing and not comparable to the context of a different situation.

It is apparent that Taleb is of the same thought-school as Karl Popper\footnote{\url{https://plato.stanford.edu/entries/popper}
\citep{KarlPopp24:online}} 
\citetext{\citealp{article-open-reproducibility-2015}; 
    \citealp{article-veronesi-2014}}.

\begin{quote}
    ``He (Karl Popper) saw falsifiability as the logical part 
    and the cornerstone of his scientific epistemology, 
    which sets the limits of scientific inquiry. 
    He proposed that statements and theories that are not 
    falsifiable are unscientific. 
    Declaring an unfalsifiable theory to be scientific would then be pseudoscience.
    `` 
    \\- \citep{wiki:karl-popper}
\end{quote}

The underpinning issue of the `replication crisis` and `rethinking economics` 
is that observations are based on the relation between the human 
and the artefact or other humans. 
In addition, the scientific models are based on observations done by humans.
This issue is also fundamental in the origin of Enterprise Engineering
\citep{article-dietz-2013} and part of the books written by J. Hoogervorst
\citetext{\citealp{article-hoogervorst-2015-sigma}; \citealp{book-hoogervorst-2017}}. 
It is the human factor that makes replication of the effect 
described or prescribed in a model or method in reality practically impossible.

\begin{quote}
``The problems experienced today are caused by `solutions` of the past. 
This has to do with the dominant 
and incorrect idea 
that there is a linear--direct--relation 
between cause and effect that are in close proximity of each other, 
in terms of time and space. `` 
\\ - \citetext{\citealp{book-senge-1990}; \citealp[p. 14]{book-janssen-2015}}. 
\end{quote}

\subsection{Replicating antifragile research}
The replication dilemma became also apparent 
when summarising the current Body of Knowledge on antifragile.
I was in search of a model of an antifragile organisation to replicate. 
This model was not found by me.

I think that there are two reasons for this: 
firstly, the field is very young, 
this can be deducted from the fact that most papers discuss 
possible applications of antifragility. 

Secondly, 
since the field is young there is not yet a clear shared mental model 
that enables the creation of models that does 
prescribe how to become antifragile.
Therefore, the studies that interact with organisations 
are applying questionnaires with retrospective questions. 
This is a method that helps in determining terminology 
that is relevant in the research context.
This is a good thing and enabled me to create a summary of
all the work done by others.

\subsection{Maturity of the field}
\label{sec:maturity}

Why do I call the current antifragile field "young"?
Recker provides us with a checklist for scientific work,
that can be used to determine the maturity of the field \citep[p. 16]{book-recker-2013} .

\begin{enumerate}
    \item Replicability
        \item Falsification
    \item Independence
    \item Precision
\end{enumerate}



\subsubsection{Recker: Replicability}

None of the research consists of research 
that can be replicated in such a way that it results 
in an antifragile organisation. 
The studies that consists only out of reasoning, 
make use of elements from the field of Complexity Science 
and therefore the reasoning can always be challenged. 
The studies that applied interviews 
make use of  a (small) set of 
companies (1 up to 5).
Interviews are difficult to replicate since they 
are always limited to: (1) the person that asks the question, 
(2) the subject that is being interviewed 
and (3) the system or context of the subject. 
The studies do prove that it is possible to use language 
from the domain of resilience and antifragility and 
relate that to the language that the subject is using 
to describe the past in that specific context.

\subsubsection{Recker: Replicability \& Falsification}

Antifragile is part of the Complex Adaptive Systems domain.
This makes it difficult to replicate and falsify the research, 
since every situation is unique and complex.
The only exception is the Chaos Engineering 
software that Netflix provided
\citep{wiki:chaosengineering}. 
The concept of the simian army toolbox
has been applied by various companies
\citep{wiki:chaosmonkey}. 
I did not find any (meta) study on the results (value creation) 
in the various companies that applied the chaos engineering concepts. 

\subsubsection{Recker: Precision}

Every research makes use of a different list of antifragile attributes.
See also section 
\ref{sec:properties} 
(\nameref{sec:properties})
and appendix 
\ref{Appendix:antifragile-attributes} 
(\nameref{Appendix:antifragile-attributes})
This results in that the definition 
what the attributes of an antifragile system is far from consistent and precise.

\subsection{Replication crisis and the relevance of this thesis}

If replication in the context of organisation design is 
(almost) impossible, what is the relevance of this study?
There are three options to contributed to science 
\citep[p. 13-14]{book-recker-2013}.
\begin{enumerate}
	\item \textbf{Theory} - How good are our explanations. 
	\\``We can improve our explanation 
	of a particular phenomenon.``
	\item \textbf{Evidence} - How good are our observations.
	\\``We can improve our collections of scientific evidence.``
	\item \textbf{Measurements} - How good are our measurements.
	\\``We can better our methods for collecting observations in relation to theory.``
\end{enumerate}

The next frog-leap in the theory domain can only be achieved when it is possible to find some evidence that there is a correlation between one (or more) of the antifragile attributes and the value of an organisation.



\subsection{Limitations of this study}
\label{sec:replication-limitation}

I did limit my research to the attributes of resilient 
and antifragile systems found in the literature. 
I did not include an in-depth research on 
why these attributes have the effect they have. 
I also did not include research on the construction 
of resilient or antifragile systems.
The model that I created based 
on the available research is nothing more
than a summary and a comprehensive model for a way-of-thinking.

\subsection{Replication possibilities of this study}


\subsubsection{Steps to replicate this study}

The steps in this research that created the model are easy to replicate. 
I think replication of my research is possible since:
\begin{enumerate}
    \item The literature list can be reproduced and expanded. 

    \item The normalisation of the collected attributes into the set of attributes that are part of the model can be reproduced.

    \item The clustering of the attributes based on the steps provided in the conceptual model are discretionary and therefore reproducible.

    \item The interviews with the experts on the validation of the model are not complex or extensive therefore easy to replicate.
    
    \item The interview with the leadership of organisations are not complex therefore easy to replicate.

\end{enumerate}
