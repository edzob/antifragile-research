%!TEX root =  ../main.tex

\newpage
\section{Enterprise Engineering}
\label{def:ee}
The discipline of Enterprise Engineering
can best be described by one of the founders of this discipline.

\begin{quote} ``Enterprise engineering is a new, 
holistic approach to address enterprise changes, of all
sizes and in all kinds of enterprises. 
Because of its holistic, systemic, approach, 
it resembles systems engineering 
\citep{book-sage-1992, book-stevens-1998}. 
But it differs from it in an important aspect: 
enterprise engineering aims to do for enterprises 
(which are basically conceived as social systems) 
what systems engineering aims to do for technical systems.`` 
\\ - \citep[p. 92]{article-dietz-2013}
\end{quote}

\subsection{Fundamentals of Enterprise Engineering}
Enterprise engineering consists out of the following fundamentals 
\citep{article-dietz-2013} 
that are part of a larger context (see figure \ref{fig:goals-ee}):
\begin{enumerate}
    \item Strict distinction between function and construction
    \item Focus on essential transactions and actors 
    \item Rigorous distinction between design and implementation 
    \item Diligent application of design principles 
    \item Distributed operational responsibility 
    \item Distributed governance responsibility 
    \item Human-centred and knowledgeable management 
\end{enumerate}
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.8\linewidth]{goals-ee.png}
    \caption{EE generic goals and fundamentals in the classification scheme by \cite{article-dietz-2013} p. 103.}
    \label{fig:goals-ee}
\end{figure}

\newpage
\subsection{Antifragility and Enterprise Engineering}

Antifragility and Enterprise Engineering 
have many overlapping views on the world.

A few examples are:
\begin{enumerate}

    \item The concepts of function and construction 
    (EE Fundamentals 1) 
    are also used in the definition of resilience types: 
    Engineering resilience, 
    Systems resilience and 
    Complex Adaptive Systems (CAS) resilience.
    \\See section \ref{sec:resilience-robust-af} 
    (\nameref{sec:resilience-robust-af}).
    
    \item Antifragile and Enterprise Engineering 
    both address the design of organisations 
    in the Complex Adaptive Systems (CAS) domain. 
    \\See section \ref{intro:where} 
    (\nameref{intro:where}).
    
    \item Antifragile and Enterprise Engineering both state 
    that organisations are in non-stop change. 
    \\See section \ref{sec:non-stop-change} 
    (\nameref{sec:non-stop-change}).
    
    \item Antifragile and Enterprise Engineering both address
    the existence and relevance of black swans or X-events. 
    \\See section \ref{sec:ee-af-blackswan} 
    (\nameref{sec:ee-af-blackswan}).
    
    \item Antifragile and Enterprise Engineering both address
    that reality is not to be understood by observation.
    \\See section \ref{sec:taleb-ee} 
    (\nameref{sec:taleb-ee}).
    
    \item Antifragile and Enterprise Engineering 
    both state that operational responsibility and 
    governance responsibility 
    should be distributed (EE Fundamentals 5 and 7).
    
    \item Antifragile and Enterprise Engineering 
    both are human-centred  
    (EE fundamentals 7).
    
    \item Antifragile and Enterprise Engineering 
    both address knowledgeable management (EE fundamentals 7).  
    Taleb denounces management, 
    by stating that `naive interventions` should be prevented.
    Taleb states that management usually is not knowledgeable 
    and therefore does more bad then good 
    \citep{book-taleb-2012}. 
    \\See section \ref{sec:naiv-intervention} 
    (\nameref{sec:naiv-intervention}).
    
    \item Antifragile and Enterprise Engineering both believe that increasing the 
    variety of the humans in the organisation 
    is the best way to deal with the VUCA world (EE fundamentals 7)
    \citep{article-hoogervorst-2015-sigma, book-taleb-2012}. 
    \\See section \ref{def:variety} (\nameref{def:variety}) 
    and section
    \ref{sec:var-cynefin} (\nameref{sec:var-cynefin}).
\end{enumerate}