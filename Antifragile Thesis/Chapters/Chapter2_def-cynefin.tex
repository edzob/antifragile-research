%!TEX root =  ../main.tex

\newpage
\section{The Cynefin Framework}
\label{def:cynefin}

The agile transformation of an organisation
is often explained with help of the Cynefin framework 
\citep{article-kurtz-2003}.
The Cynefin framework is inspired by work of Ralph D. Stacey (IBM) 
\citep{book-stacey-2001}. 
The Cynefin framework is created as a tool to be a 
'sense-making device' for managers. 
The Cynefin framework describes five domains:
(1) Simple (Obvious / Clear), 
(2) Complicated, 
(3) Complex, 
(4) Chaotic and 
(5) Disorder (Confusion).

Each domain has its own dynamic of which managers should be aware. 
In table \ref{tab:cynefin} the Cynefin domains defined and provided with the appropriate advice how to act accordingly 
\citep{wiki:cynefin}.

\begin{table}[ht]
\caption{Cynefin domains defined - \citep{wiki:cynefin}.}
\label{tab:cynefin}
\centering
\begin{tabularx}{\columnwidth}{C}    
    \hline
    \hline
    \textbf{Simple (Obvious / Clear)}\\
    \hline
    Known Knowns
    \\
    This means that there are rules in place (or best practice), 
    the situation is stable, and the relationship between cause and effect is clear. 
    \\
    The advice is to: "sense–categorize–respond"
    \\
    \hline
    \textbf{Complicated}\\
    \hline
    Known Unknowns
    \\
    The relationship between cause and effect requires analysis or expertise; 
    there are a range of right answers. Here it is possible to work rationally 
    toward a decision but doing so requires refined judgment and expertise. 
    \\
    The advice is to: "sense–analyse–respond"
    \\
    \hline
   \textbf{Complex}\\
    \hline
    Unknown Unknowns
    \\
    Cause and effect can only be deduced in retrospect, and there are no right answers. 
    "impervious to a reductionist, take-it-apart-and-see-how-it-works approach, 
    because your very actions change the situation in unpredictable ways." 
    - Thomas A. Stewart 
    \\
    The advice is to: "probe–sense–respond"
    \\ 
    \hline
    \textbf{Chaotic}\\
    \hline
    Cause and Effect are unclear\\
    Events in this domain are "too confusing to wait for a knowledge-based response" - Patrick Lambe. 
    The first and only way to respond appropriately is action. 
    \\
    The advice is to: "act–sense–respond"
    \\
    \hline
   \textbf{Disorder (Confusion)}\\
    \hline
    There is no clarity about which of the other domains apply.
    \\
    "Here, multiple perspectives jostle for prominence, factional leaders argue with one another, 
    and cacophony rules" - Snowden and Boone.
    \\
    The advice is to: break the situation down in to pieces that apply each to one of the other domains.
    \\ 
\end{tabularx}
\end{table}

\subsection{Antifragile and resilience in the Cynefin framework}

Resilience and agile are a way 
of working that fits the domain of `Complex` and `Chaotic`.
In the domain of `Complicated` and 
`Simple` planning and reductionism work best. 
In the domain of `Complex`, 
`Complex Adaptive Systems (CAS) resilience
works best.
In the domain of `Chaotic`, antifragile will work best.  
See figure \ref{fig:cynefin-cognitive-edge}.


\subsection{The Learning Organisation and the Cynefin framework}

The domain `Disorder` is applicable when 
there is no clarity about which of the other domains apply.
The domains are not measurable or absolute domains. 
In this thesis resilience and agility will be used 
as (part of) the transition from robust to antifragile. 
The definition of the domains is tightly bound to the 
understanding of the domain.
This implies that learning as a person or as an organisation
has influence on the positioning in the domain and therefore
influence on which action is to be taken.
This line of reasoning has similarities with the descriptions 
of chaos and of variety.

\subsubsection{Variety and the Cynefin framework}
\label{sec:var-cynefin}

Cynefin interlinks with the application of 
higher variety in the organisation that 
\cite{article-hoogervorst-2015-sigma} 
describes in the Enterprise Engineering theory Sigma.
An organisation in the state of order,
has a lower variety than an organisation in disorder.
By reducing the variety of an organisation,
the emergence (innovation and creativity) is also reduced. 
\cite{article-hoogervorst-2015-sigma} 
states that increasing the (internal) variety
improves the capability of an organisation 
to cope with the variety of the outside world.
\cite{article-hasan-2014} 
provide an example of this.

\begin{quote}
    ``Once the telco corporatised and indoctrinated 
    the smaller company’s personnel and practice into
    its bureaucratic culture, their innovation and creativity disappeared. 
    We suggest that if the telco wanted to support the innovation 
    and creativity they thought that they had acquired, 
    they should have allowed the group flexible and 
    self-directed arrangements more suited to unorder and
    complexity. Cynefin provides a framework within which to do this.``
    \\ - \citep{article-hasan-2014}
\end{quote}


\subsubsection{Chaos domain is inevitable}

As mentioned in section 
\ref{sec:prediction-hard} 
(\nameref{sec:prediction-hard}).
When the hyper-connected graphs that is our world
is growing in size and connections, 
then `Chaos` and `Complexity` as defined by 
\cite{article-kurtz-2003} 
in the `Cynefin framework' are inevitable to become 
a larger part of the reality we occupy.
The VUCA'ness of our reality will keep increasing.

Considering this I come to the following conclusions/advice:
\begin{itemize}
    \item Embrace the chaos in all what we do.
    \item Add tools, methods to our toolbox
    that can be utilised in the domain of `Chaos` and `Complexity`.
\end{itemize}

The goal of the model(s) presented in this thesis 
is to add tools to our toolbox for us to use 
in the `Chaos` and `Complex` domain.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.7\linewidth]{cynefin-cognitive-edge}
    \caption{Cynefin Framework by cognitive-edge by \cite{TheCynef17:online}.
    }
    \label{fig:cynefin-cognitive-edge}
\end{figure}