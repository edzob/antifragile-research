%!TEX root =  ../main.tex

\newpage
\section{Requisite Variety}
\label{def:variety}

Terms that are often used interchangeable are 
chaos, entropy and variety.
In the previous section I addressed chaos. 
In this section I address variety and entropy.

Requisite\footnote{\url{https://www.merriam-webster.com/dictionary/requisite}
\citep{requisite18:online}} 
Variety\footnote{\url{https://www.merriam-webster.com/dictionary/variety}
\citep{variety18:online}} 
is named as a system attribute
of an antifragile system
in various papers on antifragility
\citep{article-org-ghasemi-2017, 
article-org-kennon-2015, article-johnson-2013-cas}.
Requisite Variety is a law first introduced by 
\cite{article-ashby-1958}
but in the context of Enterprise Engineering the application
of Requisite Variety by 
\cite{book-beer-1979} 
is often used.
Therefore, in this section both Ashby and Beer will be used.


\subsection{Variety is relative}

The term 'variety' was introduced by 
Ashby to denote the count of the total number 
of states of a system 
\citep{book-ashby-1956, 
article-ashby-1958, wiki:variety}.

The observer and his powers of discrimination 
have a great role to play when addressing variety and 
may have to be specified 
if the variety is to be well defined 
\citep{wiki:variety}.  
Ashby and Beer both state the following
on the role of the observer:

\begin{quote}
``\textit{Variety}: Given a set of elements, 
its variety is the number of elements that can be distinguished. 
\ldots 
If two observers differ in the distinctions they can make, 
then they will differ in their estimates of the variety.''
\\ - \citep[p. 1]{article-ashby-1958}
\end{quote}

\begin{quote}
    ``(Variety is) the number of possible states of whatever it is whose complexity we want measure.``
    \\ - \citep[p. 32]{book-beer-1979}
\end{quote}

In my own words I would say that variety is subjective.

\subsection{Variety is more primitive than entropy}

As stated before, 
some people see variety and entropy as interchangeable.
Ashby states that the law of requisite variety makes use of 
concepts more primitive than those used by entropy 
\citep{article-ashby-1958}.

In statistical mechanics, 
the entropy of a system is described 
as a measure of how many different microstates there are 
\textbf{that could give rise} 
to the macrostate that the system is in. 
The entropy $S$ 
is the natural logarithm ($\ln$) 
of the number of microstates ($\Omega$), 
multiplied by the Boltzmann constant $k_\mathrm{B}$. 
This formula is also known as the 
Ludwig Boltzmann equation\footnote{Trivia fact:  
Boltzmanss's equation ($S = k Log W$) is carved on his gravestone}:
$S = k_\mathrm{B} \ln \Omega$
\citep{wiki:entropy}.


\subsection{Law of Requisite Variety}
Ashby and Beer stated the Law of Requisite Variety as 
`variety can destroy variety` 
\citep[p. 207]{book-ashby-1956}
and
`variety absorbs variety` 
\citep[p. 286]{book-beer-1979}.

Since the language used by Ashby and Beer 
is a bit difficult to apply,
the following definition is a bit more easy to understand:
'If a system is to be stable, 
the number of states of its control mechanism 
must be greater than or equal to the number of states 
in the system being controlled` 
\citep{TheLawof49:online, wiki:variety}.

\subsection{The original and elaborate Law of Requisite Variety}

The original and elaborate definition of 
the law of Requisite Variety is provided 
by \cite{book-ashby-1956} as below, 
and I want to add this 
to give a nice complete view on the origin 
of the law of Requisite Variety: 
\begin{quote}
``
\ldots simply suppose that we are watching two players, 
R and D, who are engaged in a game.
\ldots
 Let $V_D$ be the variety of D, 
$V_R$ that of R, 
and $V_O$ 
that of the outcome (all measured logarithmically).
\ldots
If $V_D$ is given and fixed,
$V_D$ – $V_R$ can be lessened 
only by a corresponding increase in
$V_R$. 
Thus the variety in the outcomes, if
minimal, can be decreased further 
only by a corresponding
increase in that of R. 
\ldots 
This is the law of Requisite Variety. 
To put it more picturesquely: 
only variety in R can force down the variety 
due to D; variety can destroy variety.
\ldots
The law of Requisite Variety says 
that R’s capacity as a regulator
cannot exceed R’s capacity as a channel of communication.
''
\\ - \citep[p. 202, 206-207, 211]{book-ashby-1956}
\end{quote}

\subsection{Variety Engineering}
\label{sec:va-af}
In the papers that state that Requisite Variety 
is part of the system attributes 
of an antifragile system, 
it is not very clear to me 
how Requisite Variety 
is seen in the 
context of those papers
\citep{article-org-ghasemi-2017, article-org-kennon-2015, article-johnson-2013-cas}.

\cite{article-org-ghasemi-2017} and 
\cite{article-org-kennon-2015} state that 
`When the number of regulators is inadequate, 
the behaviour of the system cannot 
be anticipated precisely and 
black swan events emerge. 
Thus the more regulators, the greater antifragility`. 
Stating the same as 
\cite{article-hoogervorst-2015-sigma} 
that more variety is essential for a system (or organisation) 
to deal with the world.

Interesting is that 
\cite{article-johnson-2013-cas} states
that the lack of variety in a system-of-systems 
(SoS) if fixed (or compensated) 
by the emergent behaviour of the system itself.

\begin{quote}
``\ldots 
As the actual variety becomes less than the required 
or requisite variety of the SoS, 
regulators are not able to obtain 
and exchange the requisite information 
to control the behaviour in the SoS, 
and the system becomes emergent \ldots ``
\\- \citep{article-johnson-2013-cas}.
\end{quote}

During the research into antifragility 
it became clear to me
that most design principles and/or system attributes 
defined
can be categorised in two types: 
Attenuate Variety and Amplify Variety.
These terms are part of the work on 
cybernetics of Ashby and Beer.

\textbf{Attenuate Variety} 
is the concept of reducing the variety in a system.
The absorption of change in the context of systems 
is called attenuate variety or variety reduction. 

\textbf{Amplifying Variety} 
is the concept of 
increasing the Variety in a system.
Amplifying Variety is about increasing 
the chance of a higher entropy 
and therefore being more capable to 
absorb variety increase caused by change.
Emergence leads to variety amplification.