%!TEX root =  ../main.tex

\newpage
\section{Risk Management}
\label{def:rm}

ISO 31000 (Enterprise Risk Management) 
describes Risk Management as: 
\begin{quote} 
``(Risk management is the) identification, 
assessment, and prioritisation of risks 
(effect of uncertainty on objectives, whether positive or negative) 
followed by effective and economic application 
of resources to minimise, monitor, control, 
and assure the probability 
and/or consequence of negative events 
or to maximise opportunities.``
- \citep{book-hutchins-2018, wiki:risk}.
\end{quote}

\subsection{Risk Management is about uncertainty}
\label{sec:rm-u}

Risk Management is evolving to 
acknowledging uncertainty 
\citep{article-risk-aven-2015}. 
The lack of methods to deal with `surprise` events 
has been identified by 
\cite{article-risk-Derbyshire-2014}.

\cite{article-risk-aven-2015} and 
\cite{article-makridakis-2009-forecasting}
add to the description by \cite{book-hutchins-2018}
that Risk Management is not about models 
predicting the future, 
but is about creating a system that is resilient 
in regard to events.
\begin{quote} 
    ``The risk management 
    has a special focus on situations 
    that could lead to accidents. 
    Surely, there is risk present, 
    and the challenge is to manage this in the best way. 
    This also includes to 
    \textbf{improve the understating 
    of risk by identifying signals 
    and warnings and acknowledging uncertainties 
    and the importance of knowledge}.`` 
    \\ - \citep[p. 5, 480]{article-risk-aven-2015}
\end{quote}

\cite{article-makridakis-2009-forecasting} provides a long list
that argues that risk cannot be managed by models:

\begin{quote}
    ``A huge body of empirical evidence has led to the following conclusions.
    \begin{itemize}
        \item The future is never exactly like the past. 
        This means that the extrapolation of past patterns 
        or relationships cannot provide accurate predictions.
        \item Statistically sophisticated, or complex, models 
        fit past data well but do not necessarily predict the future accurately. 
        \item “Simple” models do not necessarily fit 
        past data well but predict the future better 
        than complex or sophisticated statistical models. 
        \item Both statistical models and human judgement 
        have been unable to capture the full extent of future uncertainty. 
        People who have relied on these methods have been surprised 
        by large forecasting errors and events they did not consider. 
        \item Expert judgement is typically inferior to simple statistical models.
        \item Forecasts made by experts are no more accurate than those of knowledgeable individuals.
        \item Averaging the predictions of several individuals usually improves forecasting accuracy.
        \item Averaging the forecasts of two or more models improves accuracy while also reducing the variance of forecasting errors.``
    \end{itemize}
    - \citep{article-makridakis-2009-forecasting}
\end{quote}

\subsection{Risk Management and fragility}

\cite{article-antifragile-taleb-2013} 
state that sensitivity to risk is the same fragility:
\begin{quote}``In essence, 
fragility is the sensitivity of a given risk measure 
to an error 
in the estimation of the (possibly one-sided) 
deviation parameter of a distribution, 
especially due to the fact that the risk measure 
involves parts of the distribution – tails – 
that are away from the portion used for estimation.''
\\ - \citep[p. 7]{article-antifragile-taleb-2013} 
\end{quote}

\subsection{Risk Management and Risk Reduction}

\cite{article-risk-Derbyshire-2014} 
describe a variety of methods
that organisations apply to reduce risk. 
This summary inspired me into 
starting to search for a pattern 
in the available literature on antifragility.

These methods for risk reduction are 
(excluding the antifragile methods) :
\begin{quote}
    \begin{enumerate}
        \item ``Controlling the dispersion of outcomes
        \item Hormesis\footnote{\url{https://en.wikipedia.org/wiki/Hormesis}
        \citep{wiki:Hormesis}}
        \item Redundancy\footnote{\url{https://en.wikipedia.org/wiki/Redundancy_(engineering)} 
        \citep{wiki:Redundancy}}\textsuperscript{,}
        \footnote{\url{https://www.merriam-webster.com/dictionary/redundancy}
        \citep{redundancy18:online}}
        \item Small-scale experimentation (trial and error)``
    \end{enumerate}
    - \citep{article-risk-Derbyshire-2014}
\end{quote}

\subsection{Hormesis}
Hormesis is a not so commonly used term, 
therefore here two descriptions of this term.
\begin{quote}
    ``a theoretical phenomenon of 
    dose-response relationships 
    in which something 
    (as a heavy metal or ionizing radiation) 
    that produces harmful biological effects
    at moderate to high doses may produce 
    beneficial effects at low doses``
    \\ - \citep{hormesis18:online}
\end{quote}
\begin{quote}
    ``Hormesis is a term used by toxicologists 
    to refer to a biphasic dose response 
    to an environmental agent 
    characterised by a low dose stimulation 
    or beneficial effect and a high dose inhibitory 
    or toxic effect. 
    In the fields of biology and medicine hormesis 
    is defined as an adaptive response of cells 
    and organisms 
    to a moderate (usually intermittent) stress.`` 
    - \citep{article-mattson-2008}
\end{quote}

\subsection{Risk Management - Predicting risks is hard}

\cite{article-se-gorgeon-2015} 
summarised which factors make the prediction and identification
of risk difficult.
See also section 
\ref{sec:view-model}
(\nameref{sec:view-model}).
\begin{quote}
    \begin{enumerate}
        \item ``The future is never exactly like the past.
        \item Statistical models work well 
        if the assumptions about the probability distributions 
        are correct. 
        \item We have very often little or no knowledge 
        about the nature of the distributions of events 
        that can affect systems, positively or negatively.
        \item Research has shown extensively that 
        decision makers use heuristics to cope with the 
        complexities of estimating probabilities 
        \citep{article-tversky-1974}. 
        While these heuristics often ease the decision process, 
        they also have been shown to systematically bias judgement
        \citep{book-O'Neil-2016}.``
    \end{enumerate}
    - \cite[p. 6]{article-se-gorgeon-2015}
\end{quote}

\subsection{Risk Management - Predicting the future is hard}

Humans are not able to predict the future, 
or at least it is proven 
that we are not good in predicting the future
\citetext{\citealp{book-mintzberg-1994}; 
\citealp{article-augusto-2001}; 
\citealp{article-governatori-2007}; 
\citealp{article-makridakis-2009-forecasting};  
\citealp{article-makridakis-2009-living}; 
\citealp{article-makridakis-2009-decision}; 
\citealp[p. 17]{book-taleb-2012}; 
\citealp[p. 22]{article-org-ghasemi-2017}; 
\citealp{book-hoogervorst-2017}}.

Even if humans could predict the future,
then this prediction should be of high quality 
to have a significance
since a small deviation in the environment 
has a large (concave) effect on the fragile system 
\citep{article-se-gorgeon-2015, 
article-antifragile-taleb-2013, 
book-taleb-2007, 
book-taleb-2012}. 
See also section 
\ref{sec:pendulum} (\nameref{sec:pendulum}).

\subsection{Risk Management - Predicting the future is impossible}
\label{sec:prediction-hard}

Reality 
is a collection of hyper-connected graphs 
of which the behaviour is random 
and therefore is inherently unpredictable 
\citep{book-taleb-2001, article-pineda-2018, article-kim-2019}. 
Reality is a complex adaptive System-of-Systems 
showing a non-linear behaviour 
\citep[p. 93]{article-dietz-2013}. 

An increasing part of our reality is becoming 
unpredictable since the number of layers 
of the hyper-connected graph are increasing. 
The increase of the number of layers is 
caused by for example digital transformation. 
Digital transformation removes the physical 
boundaries the are limiting the connections between
objects and systems in the analogue world.
The number of edges and vertices in the graphs 
are also increasing by for example the 
digital communication.
A simple example of this is the network of 
connected IoT-devices.

The laws of 
Metcalfe\footnote{\url{https://en.wikipedia.org/wiki/Metcalfe'_s_law} 
\citep{wiki:Metcalfe}} 
\citep{article-metcalfe-2013} 
and 
Moore\footnote{\url{https://en.wikipedia.org/wiki/Moore's_law} 
\citep{wiki:Moore}} 
\citep{article-Waldrop-2016} 
enforce the world 
becoming completely digital and hyper-connected. 
The unstoppable increase of the number of layers 
and the number of vertices
of the hyper-connected graph 
that is our reality 
result that our reality
is and stays fundamental unpredictable.
This calls for organisations and IT systems 
that can operate and adapt within this reality.