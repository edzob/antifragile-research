%!TEX root =  ../main.tex

\chapter{Executive Summary}
\label{ManagementSummary}

% \noindent\makebox[\linewidth]{\rule{\textwidth}{0.4pt}}

 \subsection*{Turbulent times ask for resilient organisations}

\begin{quote}
    %{\large\textbf{Turbulent times ask for resilient organisations}}

	%\textbf{abstract:} 
	\textit{The .com crisis of 2000, 
	the financial crisis of 2008 and 
	the COVID-19 crisis of 2020 
	show us the importance for organisations 
	to become resilient or even antifragile to survive 
	(unexpected) external stressors.
	Research states that the current VUCA world 
	will expose enterprises to more and more stressors.}
    \textit{Resilience is the way how a system bounces back 
    from impact by a stressor. 
    Antifragile is the way a system improves by impact by a stressor.} 
    \textit{This thesis combines 
    (1) literature research on resilience with 
    (2) the antifragile attributes found in the literature and 
    (3) variety engineering into one model. 
    This model is an Extended Antifragile Attribute List (EAAL).}
    \textit{The research provided in this thesis 
    enables the leadership of an organisation 
    to determine if the organisation should aim to be 
    antifragile or to be a specific type of resilience. 
    After this determination, the EAAL model 
    enables leadership to determine which attributes 
    to include into their enterprise design.
    The EAAL is validated with various experts 
    and with the leadership of various organisations.}
    \textit{The EAAL model might also be applied to 
    generic system design including technology infrastructure, 
    software systems etc.
    The validation of the application in these domains 
    is outside the scope 
    of this research.}
\end{quote}

\noindent\makebox[\linewidth]{\rule{\textwidth}{0.4pt}}

\subsection*{The challenge}
The goal of an enterprise (e.g. organisations)
is to remain significant for its stakeholders. 
Stakeholders are owners, employees and consumers 
\citep[p. 10, 13]{book-optland-2008}.

The challenge for organisations 
is to stay relevant in the current 
\textbf{V}olatile, 
\textbf{U}ncertain, 
\textbf{C}omplex and 
\textbf{A}mbiguous (VUCA) world 
and regain their `value` 
after being disrupted 
by a black swan stressor event
\citep{book-hutchins-2018, book-mack-2015, article-bennet-2014-bhr, article-bennet-2014-bh, article-anthony-2016, book-taleb-2007, book-casti-2012}.

\subsection*{The context of the challenge}
A black swan is a stressor event in the VUCA domain 
that has a disruptive impact on a fragile and robust system and is non-predictable
\citetext{\citealp{book-taleb-2007}; \citealp{book-casti-2012}; \citealp[p. 14]{book-hole-2016}}.
The non-predictability is dependent 
on the model applied \citep{book-taleb-2007} 
and the random behaviour of the world \citep{book-taleb-2001}.
\\\textit{Summarised: 
In the perspective of an enterprise, unpredictable events will occur.}

An enterprise is an intentionally created Complex Adaptive System (CAS)
consisting of cooperating human beings
with a certain social purpose,
whereby it is impossible to 
determine the ultimate
(operational) reality of the enterprise down to the minute details
\citep[p. 93-94]{article-dietz-2013}. 
\\\textit{Summarised: 
An organisation is a system (of humans) which state is non-deterministic.}

A Complex Adaptive System is a type of a non-linear dynamical system 
in the field of Complexity Science and Systems Engineering \citep{article-lansing-2003}. 
\\\textit{Summarised: 
An organisation is a system which behaviour is non-deterministic.}

\newpage
\subsection*{Defining antifragility 
and the application on organisation design}
To deal with the VUCA world enterprises need to be resilient
\citep{article-martin-breen-2011, thesis-kastner-2017,thesis-henriksson-2016, artile-aghina-2017}. 
Agility is one of the tools to be resilient.
To survive a black swan event 
just resilience is not enough, 
for this, 
an enterprise needs to be antifragile 
\citep{book-hoogervorst-2017, book-taleb-2012}.

\begin{figure}[ht]
\centering
    \includegraphics[width=1\columnwidth]{triad4-color}
    \caption{Fragile, antifragile and robust system behaviour to stress. Resilient behaviour to time.}
    \label{fig:exsum-triad-res-v2}
\end{figure}

The Extended Antifragile Attribute List (EAAL) is a summary of the available literature validated by experts and C-level management.
This model is the first step 
in the design process 
of a resilient or antifragile organisation.
The model  
proofed to enable C-level managers 
to determine the level of resilience 
(figure \ref{fig:exsum-triad-res-v2}) 
that (parts of) the enterprise needs to have.
It enabled leaders to determine what attributes 
(figure \ref{fig:exsum-eaal-v2}) are needed 
to be able to develop the selected behaviour.

\begin{figure}[ht]
    \centering
    \includegraphics[width=1\columnwidth]{eaal}
    \caption{The EAAL model.}
    \label{fig:exsum-eaal-v2}
\end{figure}

This study can be extended 
with respect to the 
practitioner’s review by 
design authorities outside of the organisational domain.
Follow up study is needed 
on the causal relationship 
between the attributes 
and the behaviour of the system.