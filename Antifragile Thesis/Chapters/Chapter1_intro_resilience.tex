%!TEX root =  ../main.tex

\newpage
\section{Introduction into Resilience} 
\label{intro:resilience-and-robust}
\label{sec:Robust}
\label{sec:resilience-robust-af}
\label{sec:3types}
\label{sec:Resilience}


\subsection{Resilience has no clear definition}
Resilience can be defined as 
`the capability of 
a strained body to recover its size 
and shape after deformation caused especially 
by compressive stress'
\citep{Resilien18:online}.

There are various (literature) studies 
stating that there is no clear definition 
of resilience available 
\citetext{\citealp{paper-muller-2013};
    \citealp{article-martin-breen-2011};
    \citealp{article-passos-2018};
    \citealp[p. 31]{thesis-henriksson-2016};
    \citealp[section I.2.1]{thesis-kastner-2017}}.

The definitions of resilience and robust 
are varied and overlap 
\citetext{\citealp[p. 161]{article-johnson-2013-cas}; 
    \citealp[p. 178]{article-org-kennon-2015}; 
    \citealp[p. 4]{article-se-gorgeon-2015}}. 

The definitions of resilient and antifragile 
are various and overlap 
\citep{thesis-henriksson-2016, thesis-kastner-2017}.

Sources state that resilience is 
the evolution of a system 
from a robust system 
into an antifragile system 
\citetext{\citealp[p. 17]{book-taleb-2012}; 
    \citealp[p. 838]{article-florio-2014}; 
    \citealp[p. 477]{article-risk-aven-2015}; 
    \citealp[p. 23]{article-org-ghasemi-2017}; 
    \citealp[p. 7]{article-passos-2018}}.

\subsection{A definition of resilience is needed to define antifragile}
I found that the definitions of antifragile itself are also varied. 
This is the reason that a replication study of antifragility did not seem possible to me.
With this thesis 
I want to help organisations 
to become more antifragile, 
therefore I have to define what antifragility is.

Since the literature is inter-mixing (overlapping) 
definitions of resilience and antifragility,
I need to select a definition of resilience.
The selection of one definition will make it possible
to determine which elements in the current Body of Knowledge
addresses resilience and which elements address antifragility.

\subsection{Resilience definition}
\label{sec:definition-resilience}

I follow \cite{thesis-kastner-2017} 
in adopting the definition of resilience 
by \cite{article-martin-breen-2011}.

\subsubsection{Resilience is between robust and antifragile}

\cite{article-martin-breen-2011} place resilience in between robust and antifragile (see figure \ref{fig:triad}).
\begin{figure}[ht]
\centering
    \includegraphics[width=1\columnwidth]{triad-color}
    \caption{Triad of fragile, robust and antifragile.}
    \label{fig:triad}
\end{figure}

\newpage
\subsubsection{Resilience goes beyond robust 
but stops before being antifragile}

Resilience is about recovering from stressors and returning to the normal (robust) state 
(see figure \ref{fig:absorb}).
\begin{figure}[ht]
\centering
    \includegraphics[width=1\columnwidth]{absorb}
    \caption{Resilience is about absorbing and recovering.}
    \label{fig:absorb}
\end{figure}

There are three (sub-)types of resilience \citetext{\citealp[sec. I.2.2]{thesis-kastner-2017}; 
\citealp[p. 5]{article-martin-breen-2011}}.

\begin{enumerate}
\item \textbf{Engineering Resilience} or 'Common Sense' resilience. \\
The goal is to prevent disruption and changes. 
The goal is to bounce back to the fixed function/basis
\citep{thesis-kastner-2017,article-martin-breen-2011, article-holling-1996}.
Measurable by the following three characteristics: 
resistance, elasticity and stability 
\citep[p. 43]{article-martin-breen-2011}.

The function and construction 
of the system stay the same over time.

\item \textbf{Systems Resilience} (or Robustness in economics).\\
    The system has 
    the capacity to absorb disturbance 
    and reorganise while undergoing changes.
    While doing this still retain essentially the same 
    function, structure, identity, and feedback, 
    where 'essential' is defined as 
    `something functional and not identical' 
    \citetext{\citealp[p. 5]{article-walker-2004}; 
    \citealp[p. 6-7]{article-martin-breen-2011}}. 
    The system is able to withstand the impact 
    of any interruption and 
    recuperate while resuming its operations / fixed functions 
    \citep[p. 2]{article-santos-2012}.

    The function of the systems stays the same over time.
    The construction of the system may change.
    
\item \textbf{Resilience of Complex Adaptive Systems} (CAS).\\
    The system is able to become more resilient 
    and to generate new system relationships 
    by reorganisation 
    \citep{article-martin-breen-2011, thesis-kastner-2017}. 
    Function is maintained, but system structure may 
    change \citep{article-martin-breen-2011}. 
    This results in the system to being 
    as dynamic as the world around them, 
    resulting in a system that is constantly evolving
    \citep{thesis-kastner-2017}. 

    The function of the system may change over time, 
    and 
    the construction of the system may change over time.

\end{enumerate}
\begin{figure}[ht]
\centering
    \includegraphics[width=1\columnwidth]{resilience}
    \caption{Resilience type 1, 2 and 3.}
    \label{fig:resilience}
\end{figure}

\subsection{Engineering and Systems Resilience are about absorbing stressors}

\cite{thesis-kastner-2017} argues that 
the first two types of resilience are applicable 
to centralised organisations 
and that resilience is about 
\textbf{absorbing the stressors}. 

Engineering Resilience mainly absorbs the stressors 
by applying a fine-grained design and/or a strict 
command \& control structure.

Systems Resilience applies layers of systems 
to absorb the stressors. 
An example of this is backup systems or decentralised systems
like applied in the design of the internet 
\citep[p. 8]{article-passos-2018}.



\subsection{CAS Resilience is about the amplification of emergence}
\label{sec:same-organisation}

The dynamical changing of the structure 
and the function of the system 
is emergent behaviour 
which is an essential part of CAS resilience (type 3)
\citep{article-martin-breen-2011, thesis-kastner-2017}.
CAS resilience is about creating more diversity 
in the organisation. 
A question that 
\cite{thesis-kastner-2017} and 
\cite{book-taleb-2012} 
ask themselves is: 
How far can an organisation change its function and construction 
and still be the same organisation as before the change 
\citep[p. I.2.5]{thesis-kastner-2017}? 

\subsection{Resilience definition summarised}
\label{sec:resilience-def-summ}

Resilience can be summarised as:
Resilience is the behaviour of a system 
expressed in the value over time 
after a stressor event. 
The  three types of resilience are represented in figure 
\ref{fig:triad-res}.

\begin{enumerate}
    \item \textbf{Engineering Resilience} 
    is the behaviour of a system where the function 
    and the construction of the system stays the same over time.

    \item \textbf{Systems Resilience} 
    is the behaviour of the system where the function 
    of the systems stays the same over time, 
    the construction of the system may change.
    
    \item \textbf{Complex Adaptive Systems Resilience}
    is the behaviour of the system where the function 
    of the system may change 
    and the construction of the system may change over time.
\end{enumerate}
\begin{figure}[ht]
\centering
    \includegraphics[width=0.9\columnwidth]{triad4-color}
    \caption{Fragile, antifragile and robust system behaviour to stress.
    Resilience behaviour to time.}
    \label{fig:triad-res}
\end{figure}