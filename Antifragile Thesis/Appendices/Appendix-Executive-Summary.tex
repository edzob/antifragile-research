%!TEX root =  ../main.tex

\chapter{Five page Executive Summary}
\label{long-ManagementSummary}

An enterprise is an intentionally created Complex Adaptive System (CAS) 
consisting of cooperating human beings  
with a certain social purpose,
whereby it is impossible to determine the ultimate
(operational) reality of the enterprise down to the minute details
\citep[p. 93-94]{article-dietz-2013}. 
A CAS is a type of a non-linear dynamical system 
in the field of Complexity Science and Systems Engineering \citep{article-lansing-2003}.
Most, if not all, enterprises operate in a context which is described as 
Volatile, Uncertain, Complex and Ambiguous (VUCA) 
 \citep{book-hutchins-2018, book-mack-2015, article-bennet-2014-bhr, article-bennet-2014-bh}. 
Black swans (or X-events) are defined as an stressor events 
in the VUCA domain that have a disruptive impact 
on a fragile and robust system and are non-predictable
\citetext{\citealp{book-taleb-2007}; \citealp{book-casti-2012}; \citealp[p. 14]{book-hole-2016}}. 

\section{\textbf{Staying relevant as an enterprise}}

The goal of an enterprise is to remain significant 
for its stakeholders. 
Stakeholders are owners, employees and consumers 
\citep[p. 10, 13]{book-optland-2008}. 
The challenge for enterprises is to stay relevant 
in the current VUCA world 
and regain their `value` 
after being disrupted 
by a black swan stressor event. 

\subsection{\textbf{Antifragile: Staying relevant as an enterprise in a VUCA world}}

Nassim Taleb provides in his book "Antifragile" (2012) 
a way of thought 
that there is an organisation implementation 
which enables an organisation 
to gain value from the exposure to (outside) stressors. 
Antifragile organisations are placed 
within the domain of CAS \citep{article-org-kennon-2015}. 
An organisation is a System-of-Systems 
\citep{wiki:enterprise-as-a-system, book-senge-1990, book-taleb-2012}.
An Antifragile organisation is a System-of-Systems 
that contains at least one fragile and one robust system.
Taleb provides numerous examples and principles 
of antifragility.
Taleb reasons that an antifragile organisation is 
the only way to survive a black swan event and 
therefore is mandatory to stay relevant in a VUCA world \citep{book-taleb-2012}.

\section{The triad: fragile, robust and antifragile}

Antifragile is the antithesis of fragile.
Antifragile is the concept of gaining value from exposure to stressors.
Fragile is the concept of losing value from exposure to stressors. 
The concept of stressors having no effect on the value 
is called robust.    
Fragile, robust and antifragile form together a triad 
\citetext{\citealp[p. 34]{book-taleb-2012}; 
    \citealp[p. 3]{article-se-gorgeon-2015}; 
    \citealp[p. 7]{book-hole-2016}}.
Resilience is used in overlap with antifragility and robust systems. 
There is no clear definition of resilience. 
In this study we use the definition of resilience
given by \cite{article-martin-breen-2011}.  
Resilience is placed in between robust and antifragility  
(see figure \ref{fig:exsum-triad}).
\begin{figure}[ht]
\centering
    \includegraphics[width=1\columnwidth]{triad-color}
    \caption{Fragile, 
    Robust, 
    Antifragile and 
    Resilience.}
    \label{fig:exsum-triad}
\end{figure}

\section{Fragile, robust and antifragile are behaviours on stress.}
Fragile systems behave concave \& antifragile systems 
behave convex in value as response to volatility and stressors 
(see figure \ref{fig:exsum-triad-res}) 
\citep[p. 24]{article-org-ghasemi-2017}.
Robust systems are ignorant to volatility and stressors.
Fragile, robust and antifragile is behaviour of a system  
that is defined by the value as output where stress is provided as input of the system.

\section{Resilience is behaviour in time.}
Resilience is a type of a system 
between being a robust system 
and an antifragile system 
    \citetext{\citealp[p. 17]{book-taleb-2012}; 
        \citealp[p. 838]{article-florio-2014}; 
        \citealp[p. 477]{article-risk-aven-2015}; 
        \citealp[p. 4]{article-se-gorgeon-2015}; 
        \citealp[p. 23]{article-org-ghasemi-2017}; 
        \citealp[p. 7]{article-passos-2018}}.
Resilience is the behaviour of a system expressed 
in the value over time 
after an stressor event (see figure  \ref{fig:exsum-triad-res}). 
For resilience we adopt the definition provided by 
\citep[sec. I.2.2]{thesis-kastner-2017} and \citep[p. 5]{article-martin-breen-2011}.
\begin{enumerate}
    \item \textbf{Engineering Resilience} 
    is the behaviour of a system where the function and the construction of the system stays the same over time.
    \item \textbf{Systems Resilience} 
    is the behaviour of the system where the function of the systems stays the same over time, the construction of the system may change.
    \item \textbf{Complex Adaptive Systems Resilience} 
    is the behaviour of the system where the function of the system may change and the construction of the system may change over time.
\end{enumerate}

\begin{figure}[ht]
\centering
    \includegraphics[width=1\columnwidth]{triad4-color}
    \caption{Fragile systems behave concave to stress. 
    \\Antifragile systems behave convex to stress. 
    \\Robust systems are ignorant to stress.
    \\Resilience sits between robust and antifragile.}
    \label{fig:exsum-triad-res}
\end{figure}

\newpage
\section{Research on the definition of Antifragility}

Since 2012 many articles, research papers, 
master theses and books were published 
on the topic of antifragility. 
The definitions in the literature of antifragile 
and resilience  overlap. 
Most of the literature reasons which attributes 
\textit{might} 
play a role for a system to be antifragile,
or how the concept of antifragile \textit{can} 
add value to a specific domain.
Some of the existing research \textit{prescribe} 
design activities on how to create antifragile software 
as a specific system domain
\citep{article-oreilly-2019, book-zwieback-2014, wiki:chaosengineering}.

I did not find a clear set of attributes 
on how to design an antifragile organisation.
I did not find a clear 
overall set of attributes applicable to an 
antifragile system.
I provide in this thesis an overview 
of the attributes that a resilient and/or 
antifragile system needs to have. 

\subsection{Research Question}

The research questions are: 
\textit{"Can leaders determine if their organisation must be resilient or antifragile? 
    When applicable, does the “resilient \'or antifragile model” 
    support the leadership in determining what change is needed to achieve an resilient or antifragile organisation?"}

\subsection{Research Method}

This research applies the triangulation method
and consists of the following steps 
(figure \ref{fig:exsum-research}). 
\begin{enumerate}
    \item Create a model based on existing literature.
    \item Validate the model with experts.
    \item Validate the model with practitioners.
\end{enumerate}

\begin{figure}[ht]
\centering
    \includegraphics[width=1\columnwidth]{research}
    \caption{Conceptual Research Model.}
    \label{fig:exsum-research}
\end{figure}


\newpage
\subsection{Research step one 
- Create a model based on existing literature}

Most papers on antifragility discuss the 
theoretical application 
of the antifragile concept.
Most papers on antifragility combine resilience 
and antifragility 
in their list of attributes of an antifragile system.
During this research a holistic model is created. 
This model is created by grouping the attributes found 
in the literature 
applying a decision-tree. 
Followed by summarising (normalising) the attributes 
per created group.

I have created the decision-tree 
by combining
the three types of resilience provided by 
\cite{article-martin-breen-2011},
the 5\textsuperscript{th} discipline defined by 
\cite{book-senge-1990, book-floor-1999}, 
Requisite Variety as defined by 
\cite{article-ashby-1958, article-hoogervorst-2015-sigma} 
and the definition of antifragility as provided by 
\cite{book-taleb-2012}. 
This results in the model visualised in figure \ref{fig:exsum-eaal}.

\begin{figure}[ht]
    \centering
    \includegraphics[width=1\columnwidth]{eaal}
    \caption{Applying Variety Engineering \\to attributes of a resilient and antifragile system-of-systems.}
    \label{fig:exsum-eaal}
\end{figure}

\subsection{Research step two - Validate the model with experts}

The validation of the model is done by applying 
two methods.
Method one is approaching various experts 
in one-on-one conversations
and use the feedback to improve the model. 
Experts from the field of Enterprise Engineering 
(5\footnote{HM, HN, AvdH, BN, MvD}) and of Antifragility 
(5\footnote{BO, NvL, DK, DvH, CvH}) where involved. 
Method two is presenting the model to a group. 
The model was presented to two different groups. 
One group of 
three\footnote{MvS, TE, HN} 
Enterprise architects and 
one group of 
ten\footnote{MvS, TE, AvdH, MA, WdN, WdB, YC, PA, RK, MB} 
Enterprise Architects. 
Since the literature offers no concrete guidelines on methodology, models and definitions,
this double loop method is used 
to ensure that the model is consistent and 
not contrary to existing models and theories. 
By involving the Enterprise Engineering and Antifragility experts 
the double loop ensures that the model is a realistic
representation of the existing view on resilient and antifragile organisations.

\subsection{Research step three - Validate the model with Practitioners}

The model has value when it can be applied. 
Application of the model in the organisation domain 
is done by leadership that has the authority 
to (re-)design organisations.
Therefore the validation of the application 
of this model needs to be done
with leadership in organisations. 
This research applied interviews 
with five leaders separately 
and one interview with a group of two. 
The organisations were two in aviation, 
one in retail, one government, 
one NGO and two universities.

\newpage
\subsection{Research result}

This research adds to the current Body of Knowledge:
\begin{enumerate}
    \item A model (figure \ref{fig:exsum-triad}) that provides an overview 
    of the three types of resilience positioned between robust and antifragile, 
    so that behaviour of the system-of-systems can be identified.
    \item A model (figure \ref{fig:exsum-eaal}) that provides a definition of antifragility 
    and the three types of resilience.
    \item A process of validation by experts that can easily be replicated.
    \item A process of validation of application that can easily be replicated 
    and/or extended to other application domains.
\end{enumerate}

This research adds to the organisations:
\begin{enumerate}
    \item the ability to discuss and 
identify the behaviour that they see indispensable for their organisation.
    \item having a discussion and analysis of the attributes in the organisation, 
    including the sub-systems in the organisation.
\end{enumerate}

\section{Conclusion}
    
Within the limitations of the research, 
the created model provides
a definition of Antifragility based on the 
available Body of Knowledge.
This model provides insight into which
attributes are relevant for a 
resilient system-of-systems and 
which attributes are relevant 
for a antifragile system-of-systems.
The model provides inspiration with
experts and practitioners.
This model serves 
as a stepping stone
for further research and discussion 
about the design of an organisation.