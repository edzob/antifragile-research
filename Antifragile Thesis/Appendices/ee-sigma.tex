%!TEX root =  ../main.tex

\textbf{ $\beta$ is pronounced as BETA, standing for Binding Essence, Technology, and Architecture }
\begin{quote}
	``The SIGMA theory ($\sigma$) or EE governance \& management theory (SIGMA stands for Socially Inspired Governance and Management Approach), is an ideological theory about how enterprises should be managed and governed in such a way that the people in the enterprise are maximally empowered. `` - \citep{article-dietz-2014-beta}
\end{quote}

\begin{quote}
	``Traditional thinking about enterprises considers (executive) management as the primary and exclusive custodians of enterprise performance. 

	Employees, under management control, must behave instrumentally as parts of the enterprise machine. 

	Hence, no employee variety: standard, predefined instrumental behavior is often required and expected. Under the label ‘socially inspired governance and management approach’ (SIGMA) this article submits a fundamentally different perspective by arguing that, in view of enterprise operational as well as strategic performance, variety in employee behavior is crucial. 

	Moreover, the instrumental approach to employee behavior conflicts in our view with moral and ethical considerations concerning employees and society at large. 

	Our discussion proceeds as follows. In the foundational section theoretical perspectives are offered that clarify and corroborate the importance of employee variety in behavior for enterprise operational and strategic performance. This importance necessitates employee involvement and consequently employee-centric organizing. 

	Additionally, the ‘unitarist’ view on uniting employee and enterprise interests is argued, which implies the shift from traditional management behavior towards behavior that manifests leadership characteristics. 

	The subsequent section will elaborate on a few consequences of our theoretical position. Chief among these consequences is the humanization of enterprises and the affordance of meaningful work. 

	Next, we stress the importance of enterprise coherence and consistency (‘concinnity’), specifically in view of the focus on employee behavior and avoiding employee cynicism. 

	Achieving concinnity has consequences for the way enterprises are conceptualized and modeled. Dominant ways of enterprises of modeling are questioned. In view of the previous points, the last section raises issues concerning the current concepts about enterprises. 

	First, philosophical and ontological considerations will be offered in relation to the emphasized employee-centric way of organizing, followed by some observations about the current theory-base that make up the discipline of enterprise engineering. Finally, the SIGMA theory is positioned as an alternative ‘theory of business’ to the widespread economydominated theory of busi5ess.
`` - \citep{article-hoogervorst-2015-sigma}
\end{quote}


\begin{quote}
``

\ldots

The theoretical foundation of the SIGMA theory is formed by Stafford Beer’s Viable Systems Theory [1] [2], and the closely related Law of Requisite Variety formulated by Ross Ashby [3]. \ldots


\ldots

\ldots variety, which is “the measure of complexity of a system, defined as the number of its possible states” [1][op. cit., p. 13]. \ldots


\ldots

\ldots Put differently, the regulating ability of the system must have enough variety in order to address the variety it faces, since only “variety absorbs variety, and nothing else can” [1][op. cit., p. 21]. This insight refers to Ashby’s Law of Requisite Variety [3]. \ldots

\ldots Traditional ways of organizing focus on deskilling workers, hence reducing their variety
\ldots

\ldots Rather than attenuating variety and making the system rigid, the inevitable internal and external variety can be absorbed by increasing the variety of the regulator.
\ldots

\ldots Otherwise stated, the regulating capacity of the enterprise is amplified
\ldots

\ldots Evidently, “planning is a variety attenuator” [1][op. cit., p. 91].
\ldots

\ldots Planning enforces to follow predefined steps that essentially ignores variety. But organisational actions should not be based on predictions that cannot reasonable made, but based on \textbf{sensemaking} about an unfolding, emerging situation.
\ldots

\ldots First, such increase cannot be established under mechanistic conditions since these conditions limit variety. Second, as we will argue below, increase in regulating variety can only be established through the involvement of employees, which requires employee-centric organizing.
\ldots

\ldots As mentioned previously, variety is intentionally
reduced and the enterprise becomes a rigid, machine-like entity. 
\ldots

\ldots As Katz and Kahn observe, “it is impossible to prescribe role requirements precisely and completely or lay down rules with sufficient specificity to cover all contingencies arising in a single week of work of a complex organisation” on page 266 of [5].
\ldots

\ldots Noticeably, operational contingencies are also the result of ambiguity, lack of clarity, and dynamics associated with the formally defined operational roles themselves. This has, for example, to do with (1) the interpretation by the role actor of what the role is all about, (2) imposed role expectations by the actors of other organisational roles, and (3) the interpretation of the imposed role expectations by the actor receiving the expectations [5][op. cit.].
\ldots

\ldots Despite all these operational contingencies, the central characteristic of the traditional mechanistic approach is to reduce or attenuate enterprise variety through rules, directives and management control, hence, by the institutionalization or mechanization of the enterprise. Enterprises are high variety systems, whereby variety grows exponentially with the number of aspects, mentioned above, that manifest variety in various degrees: customers, suppliers, business partners, stakeholders, employees, machines, equipment, spare parts, material, information systems, work instructions, utilities, offices, buildings, etc. Since all these aspects manifest variety in an emerging manner, enterprises are clearly high-variety systems that are subject to constant perturbation due to all kinds of internal and external contingencies. \ldots 


\ldots It is here that employee involvement becomes manifest through creativity and initiatives directed at safeguarding operational process reliability, as well as directed to product, quality, and service improvements. \ldots 

\ldots In the ‘chaos’ created by operational complexity, dynamics, and uncertainty, employee involvement creates the overall order relative to the enterprise purpose and mission. Order thus emerges in an unpredictable way through a process of self-organizing.\ldots 

\ldots Obviously, organizing implies some level of formalization in the form of predefined working arrangements. However, literature on teams and their effectiveness teaches that “coordination and integration of activities can be more efficiently and effectively accomplished through human relationships and self-organizing” on page 99 of [6].\ldots 

\ldots Rather, local freedom is required to create overall order, which follow from employee actions being aligned with the enterprise purpose and goals.\ldots 

\ldots This requires guidance on higher aggregated levels, such as through enterprise culture and the meaning of work.
\ldots 

\ldots Strategic planning is merely of symbolic content; an institutionalized ritual to foster the myth of control, and secure future support, funding, and faith in the belief that the enterprise future is under intentional management control [9].
\ldots 

\ldots Empirical evidence shows however that strategic planning hardly ever led to really new innovative strategic directions [10].
\ldots 

\ldots Elsewhere we argued the importance of employee involvement for enterprise learning and innovation [11][12].
\ldots 

\ldots Strategy development follows from learning, not from planning [9]. 
\ldots 

\ldots Next to, and in conjunction with operational relationships, employees thus also have mutual cognitive, reflective, and learning relationships. These relationships are not part of formal operational models, but are ever so important for excellently performing enterprises, as well as important for the generation of new strategic ideas.
\ldots 

\ldots Underlying the traditional mechanistic, instrumental way of organizing is the assumption that employee and enterprise interests are inherently conflicting so that coercive measures and management control are required to align employee behavior with enterprise interests. This ‘dualist’ viewpoint is rejected by the so-called ‘unitarist’ viewpoint, arguing that no necessary opposition, incompatibility or divergence has to be present between enterprise and employee interests [18].
\ldots 

\ldots The unitarist view states in essence that desired forms of employee behavior based on enterprise performance, or based on considerations about human development, do not necessarily constitute a principal conflict of interest [20].
\ldots 

\ldots Under the label ‘Theory X economics’ traditional economic thinking is identified with as its central
tenet that the purpose of an enterprise is to maximize profit in order to maximize wealth for shareholders. Also employees are considered as mere rational
agents in pursuit of personal wealth, hence only driven by financial incentives.
\ldots 

\ldots On the other hand, ‘Theory Y economics’ identifies the type of thinking that considers the well-being of stakeholders, such as customers and employees and society at large, as integral parts of considerations about economic behavior of enterprises. Not the narrow pursuit of economic gain, but the pursuit of higher levels of achievement is what people naturally aspire to. Contrary to Theory X economics claiming that social responsibility and enterprise performance do not go very well together, Theory Y economics claims the opposite: both aspects are not necessarily opposite and can be aligned, as stressed before [22] [op. cit.].
\ldots 

\ldots Such alignment is considered necessary for long-term enterprise survival and growth. Moreover, such alignment itself is conducive to a long-term perspective, since unlike the short-term money-making focus, “an emphasis on the firm as a social institution generates a long-term perspective” (on page 87 of [6]).
\ldots 

\ldots Hence, “the ironic bottom line here is that focusing intently on the bottom line alone often leads to poor bottom line performance” (on page 36 of [17]).
\ldots 

\ldots The relationship between leader and followers concerns and affects the motivation of followers, based on mutual needs, expectations and values.
\ldots 

\ldots Leadership is concerned with the behavior of followers resulting from the mutually stimulating relationship.
\ldots 

\ldots While the employee-centered approach manifests itself differently in different situations,the approach itself is not considered to be situationally dependent and is based on the ideology that considers employees as the crucial core for enterprise success, since they are crucial for satisfying the Law of Requisite Variety.
\ldots 

\ldots “management must therefore explicitly acknowledge that it is at the service of people, rather than people being at the service of management” (on page 132 of [32]).
\ldots 

\ldots It is about “Moral integrity, treatment of people, people development and so on. This requires a shared value system that is difficult to achieve” [32][op. cit., p. 134]. \ldots 

\ldots The shift towards leadership implies a direction and focus towards the social aspects of organizing [21][33].
\ldots 

\ldots Enterprises are social entities engaged in purposeful action. The purpose has to be clarified and given meaning. This is a crucial aspect of leadership: “meaningmaking is the central function of leaders” (on page 85 of [6]).
\ldots 

\ldots A non-mechanistic environment must have other means than detailed rules and regulations for aligning activities towards common goal and intentions. Precisely these other means are meaning, purpose, and values.
\ldots 

\ldots As argued, satisfying the Law of Requisite Variety requires employee involvement and consequently employee-centric organizing.
\ldots 


\ldots Moreover, the espoused attention for the humanization of work at the end of the former century turned out to be a massive betrayal since the focus on finance and economics drove out moral considerations about society and employees.
\ldots 

\ldots Hence, also a similar view on enterprises is rejected. Rather, enterprises are characterized by emergence: the occurrence of novel, unpredictable phenomena.
\ldots 

\ldots The essence of the non-deterministic character of society and enterprises necessarily implies the notion of morality. Human agency and human intentionality are thus inseparable from morality [40].
\ldots 

\ldots Conversely, “belief in determinism has been the explicit denial of moral and ethical considerations in the practice of management” [40][op. cit., p. 79].
\ldots 

\ldots Since the experience of meaningful work by an employee is contingent upon specific employee characteristics or attributes, we consider the notion of meaningful work as an affordance:  a functional relationship between an employee with certain needs or desires and the enterprise with
certain properties.
\ldots 

\ldots The working environment is identified as the behavior context. The make-up of this context is clarified by the conceptual enterprise model introduced below.
\ldots 

\ldots Hence, we claim attention for the intersubjectively experienced ‘objective’ macro-level characteristics of the behavior context. Indeed, “it would be wrong, in fact morally dangerous, to assume that meaningful work is purely subjective” (on page 225 of [13]).
\ldots 

\ldots In summary, the behavior context of the employee-centric workplace must provide and manifest the following attributes:
\begin{enumerate}
	\item Purpose/meaning.
	\item Moral/ethical correctness.
	\item Achievement/personal development.
	\item Autonomy/self-efficacy.
	\item Recognition/respect.
	\item  Social belonging/relationships.
	\item Assurance/trust.
\end{enumerate}
\ldots 

\ldots since “lack of coherence in organisations leads, sooner or later, to corporate crisis” (on page 17 of [51]).
\ldots 

\ldots Likewise, practicing the unitarist view does not come from fragmented attention to some specific items, “but rather [from] a coherent, holistic pursuit in which elements reinforce one another, are inextricably intertwined and reflect a logic and a style of leadership that permeated the firm as a whole” (on page 104 of [6]).
\ldots 

\ldots Ensuring coherence and consistency of the behavioral context is thus crucial and points to a unified and integrated enterprise design (concinnity) as a necessary condition for enterprise strategic success.
\ldots 

\ldots Cases about successful enterprise transformation further show that management behavior in the form of leadership and culture (meaning, purpose, norms, and values) are the crucial determinants of success [54][55].
\ldots 


\ldots According to Burrell and Morgan, “all theories of organisation are based on a philosophy of science and a theory of society” (on page 1 of [56]).
\ldots 

\ldots Although changes in the structural functionalistic nature of an enterprise might very well be envisioned, the major thrust from enterprise change comes from changing management behavior towards leadership characteristics and changing culture.Precisely these latter two crucial aspects are excluded within the structural functionalist perspective.
\ldots 

\ldots Two conceptual meta models of enterprises are frequently used: (1) the mechanistic model, based on conceptualizing the enterprise as a machine, and (2) the organismic model, which is based on seeing the enterprise as an organism.
\ldots 

\ldots The mechanistic model is very persistent, but is fundamentally flawed. Essentially, this type of model cannot address adequately enterprise change and adaptation, as well as ignores employee agency, reflexivity and reciprocity as essential phenomena within enterprises.
\ldots 

\ldots Both the mechanistic and organismic model seem to be inadequate.
\ldots 

\ldots In view of the cognitive capacities of employees, the socio-cultural model of an enterprise is a poly-minded model: all employees can, and are expected to, address operational contingencies and contribute to enterprise strategic developments. This is the essence of distributed governance. The unique ability of a social entity to change or transform itself has been identified as morphogenic [57][58]. Also the term autopoietic is used, identifying the self-organizing capability of social entities [59][60].
\ldots 

\ldots Since enterprises are social systems, we base
the enterprise conceptual model on morphogenic social system thinking [58].
\ldots 

\ldots A social institution is an entity that affords a social function, such as the family, economic, legal, religious, education or health institutions [61].
\ldots 

\ldots Reflexivity and reciprocity play a crucial role in the nature of the relationships between human behavior, culture, and social structures.
\ldots 

\ldots The reciprocal relationships with culture can be understood in a similar vein. For example, if performance reporting only concerns productivity and efficiency, associated norms and values will develop that exclude those concerning quality and customer support and satisfaction.
\ldots 

\ldots enterprise structures and systems \ldots culture and management behavior \ldots [are] major determinants of enterprise performance and successful enterprise
change.
\ldots 

\ldots These three components of the model define the employee behavior context, and are thus the areas of attention for enterprise-wide design of employee-centric organizing.
\ldots 

\ldots This model is thereby essential for adequately conceptualizing enterprises and conducting enterprise-wide design.
\ldots 

\ldots Rejection of the strict positivist paradigm has epistemological implications in the sense that we must also reject the idea that knowledge and truth about enterprises ‘exist’ independent of enterprise members and can only be obtained through objective investigation.
\ldots 

\ldots This reflects the classic separation between object of study and the human subject (Cartesian split).
\ldots 

\ldots Core concepts are reflexivity and reciprocity: the enterprise context and employees (enterprise members in general) are in a continuous influential relationship. This viewpoint is associated with the interpretive research paradigm.
\ldots 

\ldots Knowledge and truth about enterprises is thus (also) developed through individual subjective experiences that are interpreted and made sense of, as is expressed by the sociological theory of symbolic interactionism
\ldots 

\ldots This language determines how enterprise phenomena appear. The language ‘system’ defines the available space for the interpretations that give experiences meaning and actions direction.
\ldots 

\ldots One might speak about the intersubjective foundation of objectivity, which is the ultimate source for how enterprises are experienced and perceived by employees.
\ldots 

\ldots An enterprise is to be understood in terms of individual employees and their interrelations. Only these define the ontology of an enterprise.
\ldots 

\ldots In order to evoke desired behavior, the enterprise context must be such that employee involvement is enabled. This context is a macro-level phenomenon and, as indicated above, is the core enterprise design aspect.
\ldots 

\ldots Macro-level enterprise phenomena can thus not be understood based on knowledge about individual employees because these macro-level phenomena are determined by the organisational structures and systems and the macro-level aspects of culture and group behavior, as the morphogenic enterprise system model, introduced above, expresses.
\ldots 

\ldots We agree with the fundamental insight that “there can be no social theory without an accompanying social ontology (implicit or explicit)” (on page 12 of [63]).
\ldots 

\ldots emergence – the manifestation of the unexpected and the unforeseen, which is the very reason for the essential notion of employee involvement
\ldots 

\ldots ontological dualism – the perspective that accepts both micro-level and macro-level phenomena as characterizing the nature of enterprises
\ldots 

\ldots Without ontological dualism \ldots the crucial notion of emergence \ldots cannot be acknowledged nor understood, because it is (also) the reciprocal relationship between macro-level and micro-level phenomena that brings forward emerging developments
\ldots 

\ldots “theories represent a systematic view of phenomena by specifying relations among variables using a set of interrelated constructs/variables, definitions and propositions” (on page 82 of [64]).
\ldots 

\ldots “a system of constructs and relationships between those constructs that collectively present a logical, systematic, and coherent explanation of a phenomenon of interest” (on page 46 of [65]).
\ldots 

\ldots Put differently, the current practices almost exclusively focus on structures and systems and ignore culture and management behavior as crucial determinants of enterprise performance and successful enterprise change
\ldots 

\ldots Moreover, the current practices thus ignore important aspects of enterprises being morphogenic social systems as expressed by the introduced enterprise conceptual model.
\ldots 

\ldots The exclusive structural functionalistic focus of the systemic view entails the danger of inducing a focus on stability and conservancy and runs the risk of disregarding essential sources for enterprise development and change.
\ldots 

\ldots We might observe that organisation theories of the social sciences cannot be applied properly within the structural functionalistic viewpoint only. Arguably, this viewpoint is inherently theoretically incomplete and consequently does not enable holistic, unified and integrated enterprise design. A question that concerns us is whether the current theory development within the discipline of enterprise engineering induces mechanistic thinking, precisely the type of thinking that excludes employee-centric organizing.
\ldots 

\ldots When evoking desired forms of employee behavior,the focus must be on this behavior context.
\ldots 

\ldots “rather than focusing on changing individual behaviors, the more important challenge is to change that internal environment – what we call the behavioral context – that in turn influences peoples behaviors “ (on page 142 of [66]).
\ldots 

\ldots We submit that without the effective embodiment of the traditional organisational sciences within enterprise engineering, this discipline will not mature and remains ineffective for important facets of enterprises.
\ldots 

\ldots Hence, the nature of the enterprise engineering discipline must change if the idea of the ‘socially inspired governance and management approach’ is to be taken seriously.
\ldots 

\ldots Instrumentalization of employees is necessarily associated with ‘economism’ since this viewpoint drives out any moral considerations beyond ‘making money’.
\ldots 

\ldots Unfortunately, business or management schools contributed significantly to narrow economic focus and the subsequent the social crises.
\ldots 

\ldots First, the ‘transaction cost theory’ according to which enterprises only exist because and insofar they can produce goods and services cheaper internally than acquiring them through market transactions [67]. Second the ‘agency theory’ arguing that enterprise management should act as agents for shareholders by managing enterprises such that shareholder wealth is maximized [68].
\ldots 

\ldots Central in this harmonization is the affordance of meaningful work.
\ldots 

\ldots Humanization of the workplace thus seem rather remote but is at the same time a moral imperative [51].



`` - \citep{article-hoogervorst-2015-sigma}
\end{quote}