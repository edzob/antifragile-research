# antifragile-research

This project is based around my Master project and Master thesis paper on Antifragile.
It will contain documentation on my research on the topic to get to the researchquestion up and until the final paper (hopefully)


## Contributing

Please read [CONTRIBUTING.md] for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Edzo Botjes** - *Initial work* - [Gitlab](https://gitlab.com/edzob), [Blogger](http://blog.edzob.com/), [Medium](https://medium.com/@edzob), [LinkedIN](https://www.linkedin.com/in/edzob/), [twitter](https://twitter.com/edzob) 

See also the list of [contributors](https://gitlab.com/edzob/awesome-cloud-certifications/graphs/master) who participated in this project.

## License
Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)

```
#   You are free to:
#   Share — copy and redistribute the material in any medium or format.
#   Adapt — remix, transform, and build upon the material for any purpose, even commercially.
#   The licensor cannot revoke these freedoms as long as you follow the license terms.
#    
#    You can copy, modify, distribute and perform the work,  
#    even for commercial purposes, all without asking permission.
#    
#    Under the following terms:
#    Attribution — You must give appropriate credit, provide a link to the license, 
#                  and indicate if changes were made. You may do so in any reasonable manner, 
#                  but not in any way that suggests the licensor endorses you or your use.
#    ShareAlike — If you remix, transform, or build upon the material, 
#                 you must distribute your contributions under the same license as the original.
#    No additional restrictions — You may not apply legal terms or technological measures that 
#                 legally restrict others from doing anything the license permits.
#    
#    For more see the file 'LICENSE' for copying permission.
```